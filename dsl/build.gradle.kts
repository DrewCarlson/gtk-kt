plugins {
	kotlin("multiplatform")
	`maven-publish`
	signing
	`dokka-script`
}

version = "0.1.0-alpha0"
description = "Kotlin DSL for building GTK applications"

kotlin {
	native {
		val main by compilations.getting
		binaries {
			sharedLib()
		}
	}

	sourceSets {
		val nativeMain by getting {
			dependencies {
				implementation(project(":src:gtk"))
			}
		}

	}
}

apply(plugin = "native-publish")
