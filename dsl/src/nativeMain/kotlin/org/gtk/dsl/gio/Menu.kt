package org.gtk.dsl.gio

import org.gtk.dsl.GtkDsl
import org.gtk.gio.Menu
import org.gtk.gio.MenuItem
import org.gtk.gio.MenuItem.Section
import org.gtk.gio.MenuItem.Submenu
import org.gtk.gtk.Application

/**
 * kotlinx-gtk
 * 28 / 03 / 2021
 */
@GtkDsl
fun Application.menuBar(menuBuilder: Menu.() -> Unit): Menu =
	menu(menuBuilder).also { menuBar = it }

@GtkDsl
fun menu(
	builder: Menu.() -> Unit
): Menu = Menu().apply(builder)

class SubmenuMenu : Menu()

@GtkDsl
fun SubmenuMenu.item(
	label: String,
	detailedAction: String? = null,
	builder: MenuItem.() -> Unit = {}
): MenuItem =
	MenuItem(label, detailedAction)
		.apply(builder)
		.also { appendItem(it) }

@GtkDsl
fun Menu.submenu(
	label: String?,
	builder: Submenu.() -> Unit = {},
	content: SubmenuMenu.() -> Unit = {}
): Submenu =
	Submenu(label, SubmenuMenu().apply(content))
		.apply(builder)
		.also { appendItem(it) }

@GtkDsl
fun SubmenuMenu.section(
	label: String? = null,
	builder: Section.() -> Unit = {},
	content: SubmenuMenu.() -> Unit = {}
): Section =
	Section(label, SubmenuMenu().apply(content))
		.apply(builder)
		.also { appendItem(it) }
