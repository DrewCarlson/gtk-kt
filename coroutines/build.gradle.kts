plugins {
	kotlin("multiplatform")
	`maven-publish`
}

version = "0.1.0-alpha0"
description = "Kotlin coroutines library for GTK async"

kotlin {
	native {
		val main by compilations.getting
		binaries {
			sharedLib()
		}
	}

	sourceSets {
		val nativeMain by getting {
			dependencies {
				implementation(project(":src:gtk"))
				implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0-native-mt")
			}
		}
	}
}

apply(plugin = "native-publish")