package org.gtk.gtk.ktx

import org.gtk.gtk.StringList

inline fun stringListOf(vararg elements: String): StringList =
    StringList(elements)