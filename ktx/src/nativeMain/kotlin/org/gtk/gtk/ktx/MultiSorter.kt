package org.gtk.gtk.ktx

import org.gtk.gtk.sorter.MultiSorter
import org.gtk.gtk.sorter.Sorter

/*
 * 17 / 12 / 2021
 */


inline operator fun MultiSorter.plus(sorter: Sorter) {
	append(sorter)
}