package org.gtk.gtk.ktx

import org.gtk.gtk.widgets.*

inline fun Widget.setMarginVertical(value: Int) {
	marginTop = value
	marginBottom = value
}

inline fun Widget.setMarginHorizontal(value: Int) {
	marginStart = value
	marginEnd = value
}

inline fun Widget.setMargin(value: Int) {
	marginStart = value
	marginEnd = value
	marginTop = value
	marginBottom = value
}