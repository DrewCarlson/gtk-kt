plugins {
	kotlin("multiplatform")
}

kotlin {
	native {
		val main by compilations.getting
		binaries {
			executable()
		}
	}

	sourceSets {
		val nativeMain by getting {
			dependencies {
				implementation(project(":dsl"))
				implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0-native-mt")
			}
		}
	}
}