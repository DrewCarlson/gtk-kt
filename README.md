# gtk-kt

GTK bindings for Kotlin via DSL

Testing compatible GTK Version: `4.4.1`

## Usage

See [example](example/src/nativeMain/kotlin/Main.kt) for a simple example on how to create an
application using this library

## Progress

Check out the [status](src/gtk/class-progress.ods) of the wrapping GTK

## Installation

Due to my own limitations, I am unsure of how to properly go around publishing this library onto
maven central. Instead, we will publish to mavenLocal

### Linux

#### Dependencies

Below are the command line instructions to install the needed dependencies to your distribution.
(**Note**: For Ubuntu, the GTK4 dev tools are available only from 21.04 and above.)

| Distro | Command line                                                  |
|--------|---------------------------------------------------------------|
| Fedora | ```sudo dnf install gtk4-devel ncurses-compat-libs```         |
| Ubuntu | ```sudo apt install libgtk-4-dev libncurses5  gcc-multilib``` |

### Windows

- Install [MSYS2](https://www.msys2.org/) into `C:\msys64`
- Install GTK4 dependencies `pacman -S base-devel mingw-w64-x86_64-toolchain mingw-w64-x86_64-gtk4`

### MacOS

- Install GTK4 dependencies via homebrew `brew install gtk4`

### Configure gradle

#### Repositories

```kotlin
mavenCentral()

mavenLocal() // If you want to work with the latest and greatest
```

#### Dependencies

```kotlin
// Core library
implementation("org.gtk-kt:gtk:1.0.0-alpha1")

// DSL library to make things easier to write
implementation("org.gtk-kt:dsl:0.1.0-alpha0")

// Extra libraries for convenience and interoperability with Kotlin
implementation("org.gtk-kt:coroutines:0.1.0-alpha0")
implementation("org.gtk-kt:ktx:0.1.0-alpha0")

// Base libraries if you want to work with them standalone
implementation("org.gtk-kt:cairo:0.1.0-alpha0")
implementation("org.gtk-kt:gdk-pixbuf:0.1.0-alpha0")
implementation("org.gtk-kt:gio:2.68.0-alpha0") // messed up versioning
implementation("org.gtk-kt:glib:0.1.0-alpha1")
implementation("org.gtk-kt:gobject:0.1.0-alpha1")
implementation("org.gtk-kt:pango:0.1.0-alpha0")
```

## Contribution

Read the [contribution document](CONTRIBUTING.md).

## Community

<a href="https://matrix.to/#/#gtk-kt:matrix.org">
  <img alt="Matrix" src="https://matrix.org/images/matrix-logo.svg">
</a>

## Future plans

- Split modules
- Produce guides
- Clean up code base