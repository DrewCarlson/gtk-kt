val kotlinVersion by extra("1.6.0")

plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
    gradlePluginPortal()
}

dependencies {
    implementation(kotlin("gradle-plugin", kotlinVersion))

    implementation("org.jetbrains.dokka:dokka-gradle-plugin:1.6.0")
    implementation("org.jetbrains.dokka:dokka-base:1.6.0")
}
