plugins {
	kotlin("multiplatform")
	`maven-publish`
	signing
	`dokka-script`
}

version = "0.1.0-alpha0"
description = "Kotlin bindings for Pango, targets 1.50.3"

kotlin {
	native {
		val main by compilations.getting
		val pango by main.cinterops.creating

		binaries {
			sharedLib()
		}
	}

	sourceSets {
		val nativeMain by getting {
			dependencies {
				implementation(project(":src:glib"))
				implementation(project(":src:gobject"))
			}
		}

	}
}

apply(plugin = "native-publish")
