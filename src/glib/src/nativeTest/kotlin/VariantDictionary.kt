import org.gtk.glib.Variant
import org.gtk.glib.VariantDictionary
import kotlin.test.Test

/*
 * gtk-kt
 *
 * 17 / 10 / 2021
 */

@Test
fun VariantDictionaryTest() {
	val dictionary = VariantDictionary()

	val string = "Meow"
	val int = -69
	val uInt = 69u

	val variantString = Variant(string)
	val variantInt = Variant(int)
	val variantUInt = Variant(uInt)

	assert(variantString.string == string) {
		"Preliminary string check failed"
	}

	assert(variantInt.int == int) {
		"Preliminary int check failed"
	}

	assert(variantUInt.uInt == uInt) {
		"Preliminary uInt check failed"
	}

	dictionary.insert("string", variantString)
	dictionary.insert("int", variantInt)
	dictionary.insert("uInt", variantUInt)

	val finalVariantString = dictionary.lookup("string", null)!!
	val finalVariantInt = dictionary.lookup("int", null)!!
	val finalVariantUInt = dictionary.lookup("uInt", null)!!

	assert(finalVariantString.string == string) {
		"Final string check failed"
	}

	assert(finalVariantInt.int == int) {
		"Final int check failed"
	}

	assert(finalVariantUInt.uInt == uInt) {
		"Final uInt check failed"
	}

	// Clean up
	dictionary.clear()
	dictionary.unref()
	variantString.unref()
	variantInt.unref()
	variantUInt.unref()
}