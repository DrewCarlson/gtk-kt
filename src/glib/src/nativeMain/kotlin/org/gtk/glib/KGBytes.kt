package org.gtk.glib

import glib.*
import kotlinx.cinterop.*

/**
 * @see <a href="https://docs.gtk.org/glib/struct.Bytes.html">
 *     GBytes</a>
 */
class KGBytes(
	val pointer: GBytes_autoptr,
) : Comparable<KGBytes>, UnrefMe {

	/**
	 * @see <a href=""></a>
	 */

	/**
	 * @see <a href="https://developer.gnome.org/glib/stable/glib-Byte-Arrays.html#g-bytes-new">g_bytes_new</a>
	 */
	constructor(
		data: Array<UByte>,
	) : this(
		memScoped {
			val array = allocArray<UIntVar>(data.size)

			run {
				val destArray = array.reinterpret<UByteVar>().pointed.ptr
				var index = 0
				while (index < data.size) {
					destArray[index] = data[index]
					++index
				}
			}

			g_bytes_new(
				array,
				data.size.toULong()
			)!!
		}
	)

	override fun compareTo(other: KGBytes): Int =
		g_bytes_compare(pointer, other.pointer)

	override fun equals(other: Any?): Boolean {
		if (other == null) return false
		if (this === other) return true
		if (this::class != other::class) return false

		other as KGBytes

		return g_bytes_equal(pointer, other.pointer).bool
	}

	val size: ULong
		get() = g_bytes_get_size(pointer)

	override fun hashCode(): Int = hash.toInt()

	val hash: UInt
		get() = g_bytes_hash(pointer)

	fun newFromBytes(offset: ULong, length: ULong) {
		g_bytes_new_from_bytes(
			pointer,
			offset,
			length
		)
	}

	fun ref(): KGBytes =
		g_bytes_ref(pointer)!!.wrap()

	override fun unref() {
		g_bytes_unref(pointer)
	}



	companion object {
		inline fun GBytes_autoptr?.wrap() =
			this?.wrap()

		inline fun GBytes_autoptr.wrap() =
			KGBytes(this)
	}
}