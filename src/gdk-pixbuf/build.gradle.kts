plugins {
	kotlin("multiplatform")
	`maven-publish`
	signing
	`dokka-script`
}

version = "0.1.0-alpha0"
description = "Kotlin bindings for GdpPixbuf, targets 2.42.6"

kotlin {
	native {
		val main by compilations.getting
		val pixbuf by main.cinterops.creating

		binaries {
			sharedLib()
		}
	}

	sourceSets {
		val nativeMain by getting {
			dependencies {
				implementation(project(":src:glib"))
				implementation(project(":src:gio"))
				implementation(project(":src:gobject"))
			}
		}

	}
}

apply(plugin = "native-publish")