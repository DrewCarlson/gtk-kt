package org.gtk.gtk.widgets

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gtk.Scrollable
import org.gtk.gtk.common.enums.SortType
import org.gtk.gtk.selection.SelectionModel
import org.gtk.gtk.selection.SelectionModel.Companion.wrap
import org.gtk.gtk.sorter.Sorter
import org.gtk.gtk.sorter.Sorter.Companion.wrap

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ColumnView.html">
 *     GtkColumnView</a>
 */
class ColumnView(
	val columnView: CPointer<GtkColumnView>,
) : Widget(columnView.reinterpret()), Scrollable {
	override val scrollablePointer: CPointer<GtkScrollable> by lazy {
		columnView.reinterpret()
	}

	constructor(model: SelectionModel? = null) :
			this(gtk_column_view_new(model?.selectionModelPointer)!!.reinterpret())

	fun appendColumn(column: ColumnViewColumn) {
		gtk_column_view_append_column(columnView, column.columnPointer)
	}

	val columns: ListModel
		get() = gtk_column_view_get_columns(columnView)!!.wrap()

	var isRubberbandEnabled: Boolean
		get() = gtk_column_view_get_enable_rubberband(columnView).bool
		set(value) = gtk_column_view_set_enable_rubberband(columnView, value.gtk)

	var model: SelectionModel?
		get() = gtk_column_view_get_model(columnView).wrap()
		set(value) = gtk_column_view_set_model(columnView, value?.selectionModelPointer)

	var isReorderable: Boolean
		get() = gtk_column_view_get_reorderable(columnView).bool
		set(value) = gtk_column_view_set_reorderable(columnView, value.gtk)

	var showColumnSeparators: Boolean
		get() = gtk_column_view_get_show_column_separators(columnView).bool
		set(value) = gtk_column_view_set_show_column_separators(columnView, value.gtk)

	var showRowSeparators: Boolean
		get() = gtk_column_view_get_show_row_separators(columnView).bool
		set(value) = gtk_column_view_set_show_row_separators(columnView, value.gtk)

	var singleClickActivate: Boolean
		get() = gtk_column_view_get_single_click_activate(columnView).bool
		set(value) = gtk_column_view_set_single_click_activate(columnView, value.gtk)

	val sorter: Sorter?
		get() = gtk_column_view_get_sorter(columnView).wrap()

	fun insertColumn(position: UInt, column: ColumnViewColumn) {
		gtk_column_view_insert_column(columnView, position, column.columnPointer)
	}

	fun removeColumn(column: ColumnViewColumn) {
		gtk_column_view_remove_column(columnView, column.columnPointer)
	}

	fun sortByColumn(column: ColumnViewColumn, direction: SortType) {
		gtk_column_view_sort_by_column(columnView, column.columnPointer, direction.gtk)
	}

	fun addOnActivateCallback(action: ColumnViewActivateFunction) =
		addSignalCallback(Signals.ACTIVATE, action, staticActivateCallback)

	companion object {

		inline fun CPointer<GtkColumnView>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkColumnView>.wrap() =
			ColumnView(this)

		private val staticActivateCallback: GCallback =
			staticCFunction {
					self: CPointer<GtkColumnView>,
					position: UInt,
					data: gpointer,
				->
				data.asStableRef<ColumnViewActivateFunction>()
					.get()
					.invoke(self.wrap(), position)
				Unit
			}.reinterpret()
	}
}

typealias ColumnViewActivateFunction =
		ColumnView.(
			position: UInt,
		) -> Unit