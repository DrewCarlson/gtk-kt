package org.gtk.gtk

import gtk.GtkUnit
import gtk.GtkUnit.*

/**
 * 05 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/enum.Unit.html">
 *     GtkUnit</a>
 */
enum class MeasurementUnit(
	val gtk: GtkUnit,
) {
	NONE(GTK_UNIT_NONE),
	POINTS(GTK_UNIT_POINTS),
	INCH(GTK_UNIT_INCH),
	MM(GTK_UNIT_MM);

	companion object {
		fun valueOf(gtk: GtkUnit) =
			when (gtk) {
				GTK_UNIT_NONE -> NONE
				GTK_UNIT_POINTS -> POINTS
				GTK_UNIT_INCH -> INCH
				GTK_UNIT_MM -> MM
			}
	}
}