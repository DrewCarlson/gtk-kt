package org.gtk.gtk.common.data

/**
 * 04 / 01 / 2022
 *
 * @see <a href=""></a>
 */
data class Dimensions(
	val width: Int,
	val height: Int,
)
