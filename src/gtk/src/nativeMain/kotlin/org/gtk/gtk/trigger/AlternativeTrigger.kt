package org.gtk.gtk.trigger

import gtk.GTK_TYPE_ALTERNATIVE_TRIGGER
import gtk.GtkAlternativeTrigger_autoptr
import gtk.gtk_alternative_trigger_get_first
import gtk.gtk_alternative_trigger_new
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 02 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.AlternativeTrigger.html">
 *     GtkAlternativeTrigger</a>
 */
class AlternativeTrigger(
	val altTriggerPointer: GtkAlternativeTrigger_autoptr,
) : ShortcutTrigger(altTriggerPointer.reinterpret()) {

	constructor(
		first: ShortcutTrigger,
		second: ShortcutTrigger,
	) : this(
		gtk_alternative_trigger_new(
			first.triggerPointer,
			second.triggerPointer
		)!!.reinterpret()
	)

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_ALTERNATIVE_TRIGGER))

	val first: ShortcutTrigger
		get() = gtk_alternative_trigger_get_first(altTriggerPointer)!!.wrap()

	val second: ShortcutTrigger
		get() = gtk_alternative_trigger_get_first(altTriggerPointer)!!.wrap()
}