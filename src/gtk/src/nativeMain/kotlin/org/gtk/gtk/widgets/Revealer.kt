package org.gtk.gtk.widgets

import gtk.*
import gtk.GtkRevealerTransitionType.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * kotlinx-gtk
 * 13 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Revealer.html">
 *     GtkRevealer</a>
 */
class Revealer(
	val revealerPointer: CPointer<GtkRevealer>,
) : Widget(revealerPointer.reinterpret()) {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_REVEALER))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Revealer.new.html">
	 *     gtk_revealer_new</a>
	 */
	constructor() : this(gtk_revealer_new()!!.reinterpret())

	var child: Widget?
		get() = gtk_revealer_get_child(revealerPointer).wrap()
		set(value) = gtk_revealer_set_child(revealerPointer, value?.widgetPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Revealer.get_reveal_child.html">
	 *     gtk_revealer_get_reveal_child</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Revealer.set_reveal_child.html">
	 *     gtk_revealer_set_reveal_child</a>
	 */
	var revealChild: Boolean
		get() = gtk_revealer_get_reveal_child(revealerPointer).bool
		set(value) = gtk_revealer_set_reveal_child(
			revealerPointer,
			value.gtk
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Revealer.get_child_revealed.html">
	 *     gtk_revealer_get_child_revealed</a>
	 */
	val isChildRevealed: Boolean
		get() = gtk_revealer_get_child_revealed(revealerPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Revealer.get_transition_duration.html">
	 *     gtk_revealer_get_transition_duration</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Revealer.set_transition_duration.html">
	 *     gtk_revealer_set_transition_duration</a>
	 */
	var transitionDuration: UInt
		get() = gtk_revealer_get_transition_duration(revealerPointer)
		set(value) = gtk_revealer_set_transition_duration(
			revealerPointer,
			value
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Revealer.get_transition_type.html">
	 *     gtk_revealer_get_transition_type</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Revealer.set_transition_type.html">
	 *     gtk_revealer_set_transition_type</a>
	 */
	var transitionType: TransitionType
		get() = TransitionType.valueOf(
			gtk_revealer_get_transition_type(
				revealerPointer
			)
		)!!
		set(value) = gtk_revealer_set_transition_type(
			revealerPointer,
			value.gtk
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.RevealerTransitionType.html">
	 *     GtkRevealerTransitionType</a>
	 */
	enum class TransitionType(
		val gtk: GtkRevealerTransitionType,
	) {
		NONE(GTK_REVEALER_TRANSITION_TYPE_NONE),
		CROSSFADE(GTK_REVEALER_TRANSITION_TYPE_CROSSFADE),
		SLIDE_RIGHT( GTK_REVEALER_TRANSITION_TYPE_SLIDE_RIGHT),
		SLIDE_LEFT(GTK_REVEALER_TRANSITION_TYPE_SLIDE_LEFT),
		SLIDE_UP(GTK_REVEALER_TRANSITION_TYPE_SLIDE_UP),
		SLIDE_DOWN(GTK_REVEALER_TRANSITION_TYPE_SLIDE_DOWN),
		SWING_RIGHT(GTK_REVEALER_TRANSITION_TYPE_SWING_RIGHT),
		SWING_LEFT(GTK_REVEALER_TRANSITION_TYPE_SWING_LEFT),
		SWING_UP(GTK_REVEALER_TRANSITION_TYPE_SWING_UP),
		SWING_DOWN(GTK_REVEALER_TRANSITION_TYPE_SWING_DOWN);


		companion object {
			fun valueOf(gtk: GtkRevealerTransitionType) =
				when (gtk) {
					GTK_REVEALER_TRANSITION_TYPE_NONE -> NONE
					GTK_REVEALER_TRANSITION_TYPE_CROSSFADE -> CROSSFADE
					GTK_REVEALER_TRANSITION_TYPE_SLIDE_RIGHT -> SLIDE_RIGHT
					GTK_REVEALER_TRANSITION_TYPE_SLIDE_LEFT -> SLIDE_LEFT
					GTK_REVEALER_TRANSITION_TYPE_SLIDE_UP -> SLIDE_UP
					GTK_REVEALER_TRANSITION_TYPE_SLIDE_DOWN -> SLIDE_DOWN
					GTK_REVEALER_TRANSITION_TYPE_SWING_RIGHT -> SWING_RIGHT
					GTK_REVEALER_TRANSITION_TYPE_SWING_LEFT -> SWING_LEFT
					GTK_REVEALER_TRANSITION_TYPE_SWING_UP -> SWING_UP
					GTK_REVEALER_TRANSITION_TYPE_SWING_DOWN -> SWING_DOWN
				}
		}
	}
}