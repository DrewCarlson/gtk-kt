package org.gtk.gtk.controller.gesture.single

import glib.gdouble
import glib.gpointer
import gobject.GCallback
import gtk.GTK_TYPE_GESTURE_SWIPE
import gtk.GtkGestureSwipe_autoptr
import gtk.gtk_gesture_swipe_get_velocity
import gtk.gtk_gesture_swipe_new
import kotlinx.cinterop.*
import org.gtk.glib.bool
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/signal.GestureSwipe.swipe.html">
 *     GtkGestureSwipe</a>
 */
class GestureSwipe(
	val swipePointer: GtkGestureSwipe_autoptr,
) : GestureSingle(swipePointer.reinterpret()) {

	constructor() : this(gtk_gesture_swipe_new()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_GESTURE_SWIPE))

	data class Velocity(
		val x: Double,
		val y: Double,
	)

	val velocity: Velocity?
		get() = memScoped {
			val x = cValue<DoubleVar>()
			val y = cValue<DoubleVar>()

			val r = gtk_gesture_swipe_get_velocity(swipePointer, x, y)

			if (r.bool)
				Velocity(
					x.ptr.pointed.value,
					y.ptr.pointed.value
				)
			else null
		}

	fun addOnSwipeCallback(action: GestureSwipeFunc) =
		addSignalCallback(Signals.SWIPE, action, staticSwipeFunc)

	companion object {

		inline fun GtkGestureSwipe_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkGestureSwipe_autoptr.wrap() =
			GestureSwipe(this)

		private val staticSwipeFunc: GCallback =
			staticCFunction {
					self: GtkGestureSwipe_autoptr,
					velocity_x: gdouble,
					velocity_y: gdouble,
					data: gpointer,
				->
				data.asStableRef<GestureSwipeFunc>()
					.get()
					.invoke(self.wrap(), velocity_x, velocity_y)
			}.reinterpret()
	}
}

typealias GestureSwipeFunc =
		GestureSwipe.(
			velocityX: Double,
			velocityY: Double,
		) -> Unit