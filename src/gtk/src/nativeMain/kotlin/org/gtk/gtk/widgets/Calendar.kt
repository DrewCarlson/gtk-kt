package org.gtk.gtk.widgets

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.DateTime
import org.gtk.glib.DateTime.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gobject.TypedNoArgFunc

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Calendar.html">
 *     GtkCalendar</a>
 */
class Calendar(
	val calendarPointer: GtkCalendar_autoptr,
) : Widget(calendarPointer.reinterpret()) {

	constructor() : this(gtk_calendar_new()!!.reinterpret())

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_CALENDAR))

	fun clearMarks() {
		gtk_calendar_clear_marks(calendarPointer)
	}

	val date: DateTime
		get() = gtk_calendar_get_date(calendarPointer)!!.wrap()

	fun isDayMarked(day: UInt): Boolean =
		gtk_calendar_get_day_is_marked(calendarPointer, day).bool

	var showDayNames: Boolean
		get() = gtk_calendar_get_show_day_names(calendarPointer).bool
		set(value) = gtk_calendar_set_show_day_names(calendarPointer, value.gtk)

	var showHeading: Boolean
		get() = gtk_calendar_get_show_heading(calendarPointer).bool
		set(value) = gtk_calendar_set_show_heading(calendarPointer, value.gtk)

	var showWeekNumbers: Boolean
		get() = gtk_calendar_get_show_week_numbers(calendarPointer).bool
		set(value) = gtk_calendar_set_show_week_numbers(calendarPointer, value.gtk)

	fun markDay(day: UInt) {
		gtk_calendar_mark_day(calendarPointer, day)
	}

	fun selectDay(date: DateTime) {
		gtk_calendar_select_day(calendarPointer, date.pointer)
	}

	fun unmarkDay(day: UInt) {
		gtk_calendar_unmark_day(calendarPointer, day)
	}

	fun addOnDaySelectedCallback(action: TypedNoArgFunc<Calendar>) =
		addSignalCallback(Signals.DAY_SELECTED, action, staticNoArgGCallback)

	fun addOnNextMonthCallback(action: TypedNoArgFunc<Calendar>) =
		addSignalCallback(Signals.NEXT_MONTH, action, staticNoArgGCallback)

	fun addOnNextYearCallback(action: TypedNoArgFunc<Calendar>) =
		addSignalCallback(Signals.NEXT_YEAR, action, staticNoArgGCallback)

	fun addOnPrevMonthCallback(action: TypedNoArgFunc<Calendar>) =
		addSignalCallback(Signals.PREV_MONTH, action, staticNoArgGCallback)

	fun addOnPrevYearCallback(action: TypedNoArgFunc<Calendar>) =
		addSignalCallback(Signals.PREV_YEAR, action, staticNoArgGCallback)


	companion object {
		inline fun GtkCalendar_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkCalendar_autoptr.wrap() =
			Calendar(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkCalendar_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<Calendar>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}

}