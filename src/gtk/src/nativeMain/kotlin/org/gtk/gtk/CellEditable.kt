package org.gtk.gtk

import glib.gpointer
import gobject.GCallback
import gtk.GtkCellEditable_autoptr
import gtk.gtk_cell_editable_editing_done
import gtk.gtk_cell_editable_remove_widget
import gtk.gtk_cell_editable_start_editing
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gdk.Event
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/iface.CellEditable.html">
 *     GtkCellEditable</a>
 */
interface CellEditable {
	val cellEditablePointer: GtkCellEditable_autoptr

	fun editingDone() {
		gtk_cell_editable_editing_done(cellEditablePointer)
	}

	fun removeWidget() {
		gtk_cell_editable_remove_widget(cellEditablePointer)
	}

	fun startEditing(event: Event) {
		gtk_cell_editable_start_editing(cellEditablePointer, event.eventPointer)
	}

	fun addOnEditingDoneCallback(action: TypedNoArgFunc<CellEditable>) =
		KGObject(cellEditablePointer.reinterpret())
			.addSignalCallback(Signals.EDITING_DONE, action, staticNoArgFunc)

	fun addOnRemoveWidgetCallback(action: TypedNoArgFunc<CellEditable>) =
		KGObject(cellEditablePointer.reinterpret())
			.addSignalCallback(Signals.REMOVE_WIDGET, action, staticNoArgFunc)


	companion object {
		private class Impl(
			override val cellEditablePointer: GtkCellEditable_autoptr,
		) : CellEditable

		inline fun GtkCellEditable_autoptr?.wrap() =
			this?.wrap()

		fun GtkCellEditable_autoptr.wrap(): CellEditable =
			Impl(this)

		private val staticNoArgFunc: GCallback =
			staticCFunction {
					self: GtkCellEditable_autoptr,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<CellEditable>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}
}