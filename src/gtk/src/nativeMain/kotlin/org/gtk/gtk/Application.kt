package org.gtk.gtk

import gio.GApplicationFlags
import gio.G_APPLICATION_FLAGS_NONE
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gio.Menu
import org.gtk.gio.Menu.Companion.wrap
import org.gtk.gio.MenuModel
import org.gtk.gio.MenuModel.Companion.wrap
import org.gtk.glib.WrappedKList
import org.gtk.glib.WrappedKList.Companion.asWrappedKList
import org.gtk.glib.toKArray
import org.gtk.glib.toNullTermCStringArray
import org.gtk.gobject.SignalManager
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gtk.widgets.windows.Window
import org.gtk.gtk.widgets.windows.Window.Companion.wrap

/**
 * kotlinx-gtk
 * 08 / 02 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Application.html">
 *     GtkApplication</a>
 */
open class Application(val applicationPointer: GtkApplication_autoptr) :
	org.gtk.gio.Application(applicationPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Application.new.html">
	 *     gtk_application_new</a>
	 */
	constructor(
		applicationID: String,
		flags: GApplicationFlags = G_APPLICATION_FLAGS_NONE,
	) : this(
		gtk_application_new(
			applicationID,
			flags
		)!!
	)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.add_window.html">
	 *     gtk_application_add_window</a>
	 */
	fun addWindow(window: Window) =
		gtk_application_add_window(applicationPointer, window.windowPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.get_accels_for_action.html">
	 *     gtk_application_get_accels_for_action</a>
	 */
	fun getAccelsForAction(detailedActionName: String): Array<String> =
		gtk_application_get_accels_for_action(
			applicationPointer,
			detailedActionName
		)!!.toKArray()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.get_actions_for_accel.html">
	 *     gtk_application_get_actions_for_accel</a>
	 */
	fun getActionsForAccel(accel: String): Array<String> =
		gtk_application_get_actions_for_accel(
			applicationPointer,
			accel
		)!!.toKArray()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.get_active_window.html">
	 *     gtk_application_get_active_window</a>
	 */
	val activeWindow: Window?
		get() = gtk_application_get_active_window(applicationPointer).wrap()


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.get_menu_by_id.html">
	 *     gtk_application_get_window_by_id</a>
	 */
	fun getWindowById(id: UInt): Window? =
		gtk_application_get_window_by_id(applicationPointer, id).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.get_menubar.html">
	 *     gtk_application_get_menubar</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.set_menubar.html">
	 *     gtk_application_set_menubar</a>
	 */
	var menuBar: MenuModel?
		get() =
			gtk_application_get_menubar(applicationPointer).wrap()
		set(value) {
			gtk_application_set_menubar(
				applicationPointer,
				value?.menuModelPointer
			)
		}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.get_window_by_id.html">
	 *     gtk_application_get_menu_by_id</a>
	 */
	fun getMenuById(id: String): Menu =
		gtk_application_get_menu_by_id(applicationPointer, id)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.get_windows.html">
	 *     gtk_application_get_windows</a>
	 */
	val windows: WrappedKList<Window>
		get() = gtk_application_get_windows(applicationPointer)!!.asWrappedKList(
			wrapPointer = { reinterpret<GtkWindow>().wrap() },
			getPointer = { windowPointer }
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.inhibit.html">
	 *     gtk_application_inhibit</a>
	 */
	fun inhibit(window: Window, flags: GtkApplicationInhibitFlags, reason: String): UInt =
		gtk_application_inhibit(
			applicationPointer,
			window.windowPointer,
			flags,
			reason
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.list_action_descriptions.html">
	 *     gtk_application_list_action_descriptions</a>
	 */
	val actionDescriptions: Array<String>
		get() = gtk_application_list_action_descriptions(
			applicationPointer
		)!!.toKArray()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.remove_window.html">
	 *     gtk_application_remove_window</a>
	 */
	fun removeWindow(window: Window) =
		gtk_application_remove_window(applicationPointer, window.windowPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.set_accels_for_action.html">
	 *     gtk_application_set_accels_for_action</a>
	 */
	fun setAccelsForAction(detailedActionName: String, accels: List<String>) =
		gtk_application_set_accels_for_action(
			applicationPointer,
			detailedActionName,
			accels.toNullTermCStringArray()
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Application.uninhibit.html">
	 *     gtk_application_uninhibit</a>
	 */
	fun unInhibit(cookie: UInt) =
		gtk_application_uninhibit(applicationPointer, cookie)


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Application.query-end.html">
	 *     query-end</a>
	 */
	fun addOnQueryEndCallback(action: TypedNoArgFunc<Application>): SignalManager =
		addSignalCallback(Signals.QUERY_END,action, staticNoArgGCallback)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Application.window-added.html">
	 *     window-added</a>
	 */
	fun addOnWindowAddedCallback(action: Application.(Window) -> Unit): SignalManager =
		addSignalCallback(Signals.WINDOW_ADDED, action, staticWindowEventCallback)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Application.window-removed.html">
	 *     window-removed</a>
	 */
	fun addOnWindowRemovedCallback(action: Application.(Window) -> Unit): SignalManager =
		addSignalCallback(Signals.WINDOW_REMOVED, action, staticWindowEventCallback)

	companion object {
		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkApplication_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<Application>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()

		private val staticWindowEventCallback: GCallback =
			staticCFunction {
					self: GtkApplication_autoptr,
					window: CPointer<GtkWindow>,
					data: gpointer,
				->
				data.asStableRef<Application.(Window) -> Unit>()
					.get()
					.invoke(self.wrap(), Window(window))
				Unit
			}.reinterpret()

		inline fun GtkApplication_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkApplication_autoptr.wrap() =
			Application(this)
	}
}