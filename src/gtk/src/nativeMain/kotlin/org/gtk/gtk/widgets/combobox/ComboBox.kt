package org.gtk.gtk.widgets.combobox

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.Device
import org.gtk.glib.CStringPointer
import org.gtk.glib.asStablePointer
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.*
import org.gtk.gtk.CellEditable
import org.gtk.gtk.CellLayout
import org.gtk.gtk.TreeIter
import org.gtk.gtk.TreeModel
import org.gtk.gtk.TreeModel.Companion.wrap
import org.gtk.gtk.common.callback.TreeViewRowSeparatorFunc
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gtk.common.callback.staticTreeViewRowSeparatorFunc
import org.gtk.gtk.common.enums.ScrollType
import org.gtk.gtk.common.enums.SensitivityType
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 13 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ComboBox.html">
 *     GtkComboBox</a>
 */
open class ComboBox(
	val comboBoxPointer: CPointer<GtkComboBox>,
) : Widget(comboBoxPointer.reinterpret()),
	CellEditable,
	CellLayout {

	override val cellEditablePointer: GtkCellEditable_autoptr by lazy {
		comboBoxPointer.reinterpret()
	}

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_COMBO_BOX))

	override val cellLayoutHolder: CPointer<GtkCellLayout> by lazy {
		comboBoxPointer.reinterpret()
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ComboBox.new.html">
	 *     gtk_combo_box_new</a>
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ComboBox.new_with_entry.html">
	 *     gtk_combo_box_new_with_entry</a>
	 */
	constructor(
		withEntry: Boolean = false,
	) : this((if (withEntry) {
		gtk_combo_box_new_with_entry()
	} else {
		gtk_combo_box_new()
	})!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ComboBox.new_with_model.html">
	 *     gtk_combo_box_new_with_model</a>
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ComboBox.new_with_model_and_entry.html">
	 *     gtk_combo_box_new_with_model_and_entry</a>
	 */
	constructor(
		model: TreeModel,
		withEntry: Boolean = false,
	) : this(
		(if (withEntry) {
			gtk_combo_box_new_with_model_and_entry(model.treeModelPointer)
		} else {
			gtk_combo_box_new_with_model(model.treeModelPointer)
		})!!.reinterpret()
	)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.get_active.html">
	 *     gtk_combo_box_get_active</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.set_active.html">
	 *     gtk_combo_box_set_active</a>
	 */
	var active: Int
		get() = gtk_combo_box_get_active(comboBoxPointer)
		set(value) = gtk_combo_box_set_active(comboBoxPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.get_active_id.html">
	 *     gtk_combo_box_get_active_id</a>
	 */
	val activeId: String?
		get() = gtk_combo_box_get_active_id(comboBoxPointer)?.toKString()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.get_active_iter.html"></a>
	 */
	fun getActiveIter(treeIter: TreeIter): Boolean =
		gtk_combo_box_get_active_iter(comboBoxPointer, treeIter.treeIterPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.get_button_sensitivity.html">
	 *     gtk_combo_box_get_button_sensitivity</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.set_button_sensitivity.html">
	 *     gtk_combo_box_set_button_sensitivity</a>
	 */
	var buttonSensitivity: SensitivityType
		get() = SensitivityType.valueOf(gtk_combo_box_get_button_sensitivity(comboBoxPointer))
		set(value) = gtk_combo_box_set_button_sensitivity(comboBoxPointer, value.gtk)

	var child: Widget?
		get() = gtk_combo_box_get_child(comboBoxPointer).wrap()
		set(value) = gtk_combo_box_set_child(comboBoxPointer, value?.widgetPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.get_entry_text_column.html">
	 *     gtk_combo_box_get_entry_text_column</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.set_entry_text_column.html">
	 *     gtk_combo_box_set_entry_text_column</a>
	 */
	var entryTextColumn: Int
		get() = gtk_combo_box_get_entry_text_column(comboBoxPointer)
		set(value) = gtk_combo_box_set_entry_text_column(comboBoxPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.get_has_entry.html">
	 *     gtk_combo_box_get_has_entry</a>
	 */
	val hasEntry
		get() = gtk_combo_box_get_has_entry(comboBoxPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.get_id_column.html">
	 *     gtk_combo_box_get_id_column</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.set_id_column.html">
	 *     gtk_combo_box_set_id_column</a>
	 */
	var idColumn: Int
		get() = gtk_combo_box_get_id_column(comboBoxPointer)
		set(value) = gtk_combo_box_set_id_column(comboBoxPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.get_model.html">
	 *     gtk_combo_box_get_model</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.set_model.html">
	 *     gtk_combo_box_set_model</a>
	 */
	var model: TreeModel?
		get() = gtk_combo_box_get_model(comboBoxPointer)?.wrap()
		set(value) = gtk_combo_box_set_model(comboBoxPointer, value?.treeModelPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.get_popup_fixed_width.html">
	 *     gtk_combo_box_get_popup_fixed_width</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.set_popup_fixed_width.html">
	 *     gtk_combo_box_set_popup_fixed_width</a>
	 */
	var popupFixedWidth: Boolean
		get() = gtk_combo_box_get_popup_fixed_width(comboBoxPointer).bool
		set(value) = gtk_combo_box_set_popup_fixed_width(
			comboBoxPointer,
			value.gtk
		)

	/*
	Cannot get the function, as that would point to internal static function
	@see <a href="https://docs.gtk.org/gtk4/method.ComboBox.get_row_separator_func.html"></a>
	 */

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.popdown.html">
	 *     gtk_combo_box_popdown</a>
	 */
	fun popdown() {
		gtk_combo_box_popdown(comboBoxPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.popup.html">
	 *     gtk_combo_box_popup</a>
	 */
	fun popup() {
		gtk_combo_box_popup(comboBoxPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.popup_for_device.html">
	 *     gtk_combo_box_popup_for_device</a>
	 */
	fun popupForDevice(device: Device) {
		gtk_combo_box_popup_for_device(comboBoxPointer, device.pointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.set_active_id.html">
	 *     gtk_combo_box_set_active_id</a>
	 */
	fun setActiveId(activeID: String): Boolean =
		gtk_combo_box_set_active_id(comboBoxPointer, activeID).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.set_active_iter.html">
	 *     gtk_combo_box_set_active_iter</a>
	 */
	fun setActiveIter(iter: TreeIter) {
		gtk_combo_box_set_active_iter(comboBoxPointer, iter.treeIterPointer)
	}


	/**
	 * No point to getting the row separator function. Store it yourself.
	 *
	 * @param function Separator function, set as null to remove
	 * @see <a href="https://docs.gtk.org/gtk4/method.ComboBox.set_row_separator_func.html">
	 *     gtk_combo_box_set_row_separator_func</a>
	 */
	fun setRowSeparatorFunc(function: TreeViewRowSeparatorFunc?) {
		if (function == null) {
			gtk_combo_box_set_row_separator_func(
				comboBoxPointer,
				null,
				null,
				null
			)
			return
		}
		gtk_combo_box_set_row_separator_func(
			comboBoxPointer,
			staticTreeViewRowSeparatorFunc,
			function.asStablePointer(),
			staticDestroyStableRefFunction
		)
	}

	fun addOnActivateCallback(action: TypedNoArgFunc<ComboBox>) =
		addSignalCallback(Signals.ACTIVATE, action, staticNoArgFunc)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.ComboBox.changed.html">
	 *     changed</a>
	 */
	fun addOnChangedCallback(action: TypedNoArgFunc<ComboBox>): SignalManager =
		addSignalCallback(Signals.CHANGED, action, staticNoArgFunc)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.ComboBox.format-entry-text.html">
	 *     format-entry-text</a>
	 */
	fun addOnFormatEntryTextCallback(action: ComboBoxFormatEntryTextFunction): SignalManager =
		addSignalCallback(Signals.FORMAT_ENTRY_TEXT, action, staticFormatEntryFunction)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.ComboBox.move-active.html">
	 *     move-active</a>
	 */
	fun addOnMoveActiveCallback(action: ComboBoxMoveActiveFunction): SignalManager =
		addSignalCallback(Signals.MOVE_ACTIVE, action, staticMoveActiveFunction)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.ComboBox.popdown.html">
	 *     popdown</a>
	 */
	fun addOnPopdownCallback(action: TypedNoArgFunc<ComboBox>): SignalManager =
		addSignalCallback(Signals.POPDOWN, action, staticNoArgFunc)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.ComboBox.popup.html">
	 *     popup</a>
	 */
	fun addOnPopupCallback(action: TypedNoArgFunc<ComboBox>): SignalManager =
		addSignalCallback(Signals.POPUP, action, staticNoArgFunc)

	companion object {
		inline fun GtkComboBox_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkComboBox_autoptr.wrap() =
			ComboBox(this)

		private val staticNoArgFunc: GCallback =
			staticCFunction {
					self: GtkComboBox_autoptr,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<ComboBox>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()

		private val staticFormatEntryFunction: GCallback =
			staticCFunction {
					self: GtkComboBox_autoptr,
					path: CStringPointer,
					data: gpointer,
				->
				memScoped {
					data.asStableRef<ComboBoxFormatEntryTextFunction>().get()
						.invoke(self.wrap(), path.toKString()).cstr.ptr
				}
			}.reinterpret()

		private val staticMoveActiveFunction: GCallback =
			staticCFunction {
					self: GtkComboBox_autoptr,
					type: GtkScrollType,
					data: gpointer,
				->
				data.asStableRef<ComboBoxMoveActiveFunction>().get()
					.invoke(self.wrap(), ScrollType.valueOf(type)!!)
				Unit
			}.reinterpret()
	}
}

/**
 * @see <a href="https://docs.gtk.org/gtk4/signal.ComboBox.move-active.html">
 *     move-active</a>
 */
typealias ComboBoxMoveActiveFunction = ComboBox.(ScrollType) -> Unit

/**
 * @see <a href="https://docs.gtk.org/gtk4/signal.ComboBox.format-entry-text.html">
 *     format-entry-text</a>
 */
typealias ComboBoxFormatEntryTextFunction = ComboBox.(@ParameterName("path") String) -> String