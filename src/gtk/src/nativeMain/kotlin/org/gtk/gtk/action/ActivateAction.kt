package org.gtk.gtk.action

import gtk.GTK_TYPE_SHORTCUT_ACTION
import gtk.GtkActivateAction_autoptr
import gtk.gtk_activate_action_get
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * gtk-kt
 *
 * 19 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ActivateAction.html">
 *     GtkActivateAction</a>
 */
class ActivateAction(val activateAction: GtkActivateAction_autoptr) :
	ShortcutAction(activateAction.reinterpret()) {

	constructor() : this(gtk_activate_action_get()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_SHORTCUT_ACTION))

	companion object {
		inline fun GtkActivateAction_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkActivateAction_autoptr.wrap() =
			ActivateAction(this)
	}
}