package org.gtk.gtk.controller.gesture.single

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.glib.bool
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * gtk-kt
 *
 * 26 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GestureDrag.html">
 *     GtkGestureDrag</a>
 */
open class GestureDrag(
	val gestureDragPointer: GtkGestureDrag_autoptr,
) : GestureSingle(gestureDragPointer.reinterpret()) {

	val offset: Pair<Double, Double>?
		get() = memScoped {
			val x = cValue<DoubleVar>()
			val y = cValue<DoubleVar>()

			if (gtk_gesture_drag_get_offset(gestureDragPointer, x, y).bool)
				x.ptr.pointed.value to y.ptr.pointed.value
			else null
		}
	val startPoint: Pair<Double, Double>?
		get() = memScoped {
			val x = cValue<DoubleVar>()
			val y = cValue<DoubleVar>()

			if (gtk_gesture_drag_get_start_point(gestureDragPointer, x, y).bool)
				x.ptr.pointed.value to y.ptr.pointed.value
			else null
		}

	constructor() : this(gtk_gesture_drag_new()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_GESTURE_DRAG))

	fun addOnDragBeginCallback(action: GestureDragStartFunction) =
		addSignalCallback(Signals.DRAG_BEGIN, action, staticGestureStartFunction)

	fun addOnDragEndCallback(action: GestureDragOffsetFunction) =
		addSignalCallback(Signals.DRAG_END, action, staticGestureOffsetFunction)

	fun addOnDragUpdateCallback(action: GestureDragOffsetFunction) =
		addSignalCallback(Signals.DRAG_UPDATE, action, staticGestureOffsetFunction)


	companion object {

		inline fun GtkGestureDrag_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkGestureDrag_autoptr.wrap() =
			GestureDrag(this)

		private val staticGestureStartFunction: GCallback =
			staticCFunction {
					self: GtkGestureDrag_autoptr,
					startX: Double,
					startY: Double,
					data: gpointer,
				->
				data.asStableRef<GestureDragStartFunction>()
					.get()
					.invoke(self.wrap(), startX, startY)
			}.reinterpret()

		private val staticGestureOffsetFunction: GCallback =
			staticCFunction {
					self: GtkGestureDrag_autoptr,
					offsetX: Double,
					offsetY: Double,
					data: gpointer,
				->
				data.asStableRef<GestureDragOffsetFunction>()
					.get()
					.invoke(self.wrap(), offsetX, offsetY)
			}.reinterpret()
	}
}

typealias GestureDragStartFunction =
		GestureDrag.(startX: Double, startY: Double) -> Unit

typealias GestureDragOffsetFunction =
		GestureDrag.(offsetX: Double, offsetY: Double) -> Unit
