package org.gtk.gtk.trigger

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.glib.Variant
import org.gtk.glib.Variant.Companion.wrap
import org.gtk.gobject.KGObject
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.ShortcutAction
import org.gtk.gtk.ShortcutAction.Companion.wrap
import org.gtk.gtk.trigger.ShortcutTrigger.Companion.wrap

/**
 * 25 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Shortcut.html">
 *     GtkShortcut</a>
 */
class Shortcut(
	val shortcutPointer: GtkShortcut_autoptr,
) : KGObject(shortcutPointer.reinterpret()) {

	constructor(obj: KGObject) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_SHORTCUT))

	constructor(
		trigger: ShortcutTrigger?,
		action: ShortcutAction?,
	) : this(
		gtk_shortcut_new(
			trigger?.triggerPointer,
			action?.actionPointer
		)!!
	)

	var action: ShortcutAction?
		get() = gtk_shortcut_get_action(shortcutPointer).wrap()
		set(value) = gtk_shortcut_set_action(shortcutPointer, value?.actionPointer)

	var arguments: Variant?
		get() = gtk_shortcut_get_arguments(shortcutPointer).wrap()
		set(value) = gtk_shortcut_set_arguments(shortcutPointer, value?.variantPointer)

	var trigger: ShortcutTrigger?
		get() = gtk_shortcut_get_trigger(shortcutPointer).wrap()
		set(value) = gtk_shortcut_set_trigger(shortcutPointer, value?.triggerPointer)
}