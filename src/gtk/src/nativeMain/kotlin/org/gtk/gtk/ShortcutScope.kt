package org.gtk.gtk

import gtk.GtkShortcutScope
import gtk.GtkShortcutScope.*

/**
 * 26 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/enum.ShortcutScope.html">
 *     GtkShortcutScope</a>
 */
enum class ShortcutScope(
	val gtk: GtkShortcutScope,
) {
	LOCAL(GTK_SHORTCUT_SCOPE_LOCAL),
	MANAGED(GTK_SHORTCUT_SCOPE_MANAGED),
	GLOBAL(GTK_SHORTCUT_SCOPE_GLOBAL);

	companion object {
		fun valueOf(gtk: GtkShortcutScope) =
			when (gtk) {
				GTK_SHORTCUT_SCOPE_LOCAL -> LOCAL
				GTK_SHORTCUT_SCOPE_MANAGED -> MANAGED
				GTK_SHORTCUT_SCOPE_GLOBAL -> GLOBAL
			}
	}
}