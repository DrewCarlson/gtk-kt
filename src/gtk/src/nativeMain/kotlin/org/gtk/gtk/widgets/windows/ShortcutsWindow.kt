package org.gtk.gtk.widgets.windows

import glib.gpointer
import gobject.GCallback
import gtk.GTK_TYPE_SHORTCUTS_WINDOW
import gtk.GtkShortcutsWindow
import gtk.GtkShortcutsWindow_autoptr
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 * 07 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ShortcutsWindow.html">
 *     GtkShortcutsWindow</a>
 */
class ShortcutsWindow(
	@Suppress("MemberVisibilityCanBePrivate")
	val shortCutsWindowPointer: GtkShortcutsWindow_autoptr,
) : Window(shortCutsWindowPointer.reinterpret()) {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget,
		GTK_TYPE_SHORTCUTS_WINDOW))

	fun addOnCloseCallback(action: TypedNoArgFunc<ShortcutsWindow>) =
		addSignalCallback(Signals.CLOSE, action, staticNoArgGCallback)

	fun addOnSearchCallback(action: TypedNoArgFunc<ShortcutsWindow>) =
		addSignalCallback(Signals.SEARCH, action, staticNoArgGCallback)

	companion object {
		inline fun CPointer<GtkShortcutsWindow>.wrap() =
			ShortcutsWindow(this)

		inline fun CPointer<GtkShortcutsWindow>?.wrap() =
			this?.wrap()

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkShortcutsWindow_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<ShortcutsWindow>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}
}