package org.gtk.gtk.manager

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gtk.common.enums.BaselinePosition

/**
 * kotlinx-gtk
 *
 * 22 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GridLayout.html">
 *     GtkGridLayout</a>
 */
class GridLayout(
	val gridLayoutPointer: GtkGridLayout_autoptr
) : LayoutManager(gridLayoutPointer.reinterpret()) {

	constructor() : this(gtk_grid_layout_new()!!.reinterpret())

	var baselineRow: Int
		get() = gtk_grid_layout_get_baseline_row(gridLayoutPointer)
		set(value) = gtk_grid_layout_set_baseline_row(gridLayoutPointer, value)

	var columnHomogeneous: Boolean
		get() = gtk_grid_layout_get_column_homogeneous(
			gridLayoutPointer
		).bool
		set(value) = gtk_grid_layout_set_column_homogeneous(
			gridLayoutPointer, value.gtk
		)

	var columnSpacing: UInt
		get() = gtk_grid_layout_get_column_spacing(gridLayoutPointer)
		set(value) = gtk_grid_layout_set_column_spacing(gridLayoutPointer, value)

	fun getRowBaselinePosition(row: Int): BaselinePosition =
		BaselinePosition.valueOf(
			gtk_grid_layout_get_row_baseline_position(gridLayoutPointer, row)
		)

	var rowHomogeneous: Boolean
		get() = gtk_grid_layout_get_row_homogeneous(
			gridLayoutPointer
		).bool
		set(value) = gtk_grid_layout_set_row_homogeneous(
			gridLayoutPointer, value.gtk
		)

	var rowSpacing: UInt
		get() = gtk_grid_layout_get_row_spacing(gridLayoutPointer)
		set(value) = gtk_grid_layout_set_row_spacing(gridLayoutPointer, value)

	fun setRowBaselinePosition(row: Int, position: BaselinePosition) {
		gtk_grid_layout_set_row_baseline_position(
			gridLayoutPointer,
			row,
			position.gtk
		)
	}
}