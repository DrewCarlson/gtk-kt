package org.gtk.gtk

import gtk.gtk_get_major_version
import gtk.gtk_get_micro_version
import gtk.gtk_get_minor_version

/**
 * gtk-kt
 *
 * 24 / 09 / 2021
 *
 * @see <a href=""></a>
 */

val gtkMajorVersion
	get() = gtk_get_major_version()

val gtkMinorVersion
	get() = gtk_get_minor_version()

val gtkMicroVersion
	get() = gtk_get_micro_version()