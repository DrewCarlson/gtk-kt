package org.gtk.gtk.widgets.popover

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gio.MenuModel
import org.gtk.gio.MenuModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.gtk.widgets.Widget

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.PopoverMenu.html">
 *     GtkPopoverMenu</a>
 */
class PopoverMenu(
	val popoverMenuPointer: CPointer<GtkPopoverMenu>,
) : Popover(popoverMenuPointer.reinterpret()) {

	constructor(model: MenuModel) :
			this(gtk_popover_menu_new_from_model(model.menuModelPointer)!!.reinterpret())

	constructor(model: MenuModel, flags: GtkPopoverMenuFlags) :
			this(gtk_popover_menu_new_from_model_full(model.menuModelPointer,
				flags)!!.reinterpret())

	fun addChild(child: Widget, id: String): Boolean =
		gtk_popover_menu_add_child(popoverMenuPointer, child.widgetPointer, id).bool

	var menuModel: MenuModel
		get() = gtk_popover_menu_get_menu_model(popoverMenuPointer)!!.wrap()
		set(value) = gtk_popover_menu_set_menu_model(popoverMenuPointer, value.menuModelPointer)

	fun removeChild(child: Widget): Boolean =
		gtk_popover_menu_remove_child(popoverMenuPointer, child.widgetPointer).bool
}