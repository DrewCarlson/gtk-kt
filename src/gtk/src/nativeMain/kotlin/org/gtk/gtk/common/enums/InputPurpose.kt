package org.gtk.gtk.common.enums;

import gtk.GtkInputPurpose
import gtk.GtkInputPurpose.*

/**
 * @see <a href="https://docs.gtk.org/gtk4/enum.InputPurpose.html">
 *     GtkInputPurpose</a>
 */
enum class InputPurpose(val gtk: GtkInputPurpose) {
	/**
	 * Allow any character
	 */
	FREE_FORM(GTK_INPUT_PURPOSE_FREE_FORM),

	/**
	 * Allow only alphabetic characters
	 */
	ALPHA(GTK_INPUT_PURPOSE_ALPHA),

	/**
	 * Allow only digits
	 */
	DIGITS(GTK_INPUT_PURPOSE_DIGITS),

	/**
	 * Edited field expects numbers
	 */
	NUMBER(GTK_INPUT_PURPOSE_NUMBER),

	/**
	 * Edited field expects phone number
	 */
	PHONE(GTK_INPUT_PURPOSE_PHONE),

	/**
	 * Edited field expects URL
	 */
	URL(GTK_INPUT_PURPOSE_URL),

	/**
	 * Edited field expects email address
	 */
	EMAIL(GTK_INPUT_PURPOSE_EMAIL),

	/**
	 * Edited field expects the name of a person
	 */
	NAME(GTK_INPUT_PURPOSE_NAME),

	/**
	 * Like [FREE_FORM], but characters are hidden
	 */
	PASSWORD(GTK_INPUT_PURPOSE_PASSWORD),

	/**
	 * Like [DIGITS], but characters are hidden
	 */
	PIN(GTK_INPUT_PURPOSE_PIN),

	/**
	 * Allow any character, in addition to control codes
	 */
	TERMINAL(GTK_INPUT_PURPOSE_TERMINAL);

	companion object {
		fun valueOf(gtk: GtkInputPurpose) =
			when (gtk) {
				GTK_INPUT_PURPOSE_FREE_FORM -> FREE_FORM
				GTK_INPUT_PURPOSE_ALPHA -> ALPHA
				GTK_INPUT_PURPOSE_DIGITS -> DIGITS
				GTK_INPUT_PURPOSE_NUMBER -> NUMBER
				GTK_INPUT_PURPOSE_PHONE -> PHONE
				GTK_INPUT_PURPOSE_URL -> URL
				GTK_INPUT_PURPOSE_EMAIL -> EMAIL
				GTK_INPUT_PURPOSE_NAME -> NAME
				GTK_INPUT_PURPOSE_PASSWORD -> PASSWORD
				GTK_INPUT_PURPOSE_PIN -> PIN
				GTK_INPUT_PURPOSE_TERMINAL -> TERMINAL
			}
	}
}