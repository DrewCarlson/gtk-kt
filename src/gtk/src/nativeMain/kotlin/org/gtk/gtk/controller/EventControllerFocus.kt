package org.gtk.gtk.controller

import glib.gpointer
import gobject.GCallback
import gtk.GtkEventControllerFocus
import gtk.gtk_event_controller_focus_contains_focus
import gtk.gtk_event_controller_focus_is_focus
import gtk.gtk_event_controller_focus_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.bool
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.TypedNoArgFunc

/**
 * 04 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.EventControllerFocus.html"></a>
 */
class EventControllerFocus(
	val focusPointer: CPointer<GtkEventControllerFocus>,
) : EventController(focusPointer.reinterpret()) {

	constructor() : this(gtk_event_controller_focus_new()!!.reinterpret())

	val containsFocus: Boolean
		get() = gtk_event_controller_focus_contains_focus(
			focusPointer
		).bool

	val isFocus: Boolean
		get() = gtk_event_controller_focus_is_focus(focusPointer).bool

	fun addOnEnterCallback(action: TypedNoArgFunc<EventControllerFocus>) =
		addSignalCallback(Signals.ENTER, action, staticNoArgGCallback)

	fun addOnLeaveCallback(action: TypedNoArgFunc<EventControllerFocus>) =
		addSignalCallback(Signals.LEAVE, action, staticNoArgGCallback)

	companion object {
		inline fun CPointer<GtkEventControllerFocus>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkEventControllerFocus>.wrap() =
			EventControllerFocus(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction {
					self: CPointer<GtkEventControllerFocus>,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<EventControllerFocus>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}
}