package org.gtk.gtk.selection

import gio.GListModel_autoptr
import gobject.GObject
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gobject.KGObject.Companion.wrap

/**
 * gtk-kt
 *
 * 30 / 10 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.SingleSelection.html">GtkSingleSelection</a>
 */
class SingleSelection(
	val singleSelectionPointer: CPointer<GtkSingleSelection>
) : SelectionModel {

	override val listModelPointer: GListModel_autoptr by lazy {
		singleSelectionPointer.reinterpret()
	}

	override val selectionModelPointer: GtkSelectionModel_autoptr by lazy {
		singleSelectionPointer.reinterpret()
	}

	constructor(model: ListModel?) :
			this(gtk_single_selection_new(model?.listModelPointer)!!)

	var autoSelect: Boolean
		get() = gtk_single_selection_get_autoselect(singleSelectionPointer).bool
		set(value) = gtk_single_selection_set_autoselect(
			singleSelectionPointer,
			value.gtk
		)

	var canUnselect: Boolean
		get() = gtk_single_selection_get_can_unselect(singleSelectionPointer).bool
		set(value) = gtk_single_selection_set_can_unselect(
			singleSelectionPointer,
			value.gtk
		)

	var model: ListModel?
		get() = gtk_single_selection_get_model(singleSelectionPointer)?.wrap()
		set(value) = gtk_single_selection_set_model(
			singleSelectionPointer,
			value?.listModelPointer
		)

	val selectedItem: KGObject?
		get() = gtk_single_selection_get_selected_item(singleSelectionPointer)
			?.reinterpret<GObject>().wrap()

}