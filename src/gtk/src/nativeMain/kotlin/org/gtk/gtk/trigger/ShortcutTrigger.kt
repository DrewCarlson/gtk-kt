package org.gtk.gtk.trigger

import gtk.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gdk.Display
import org.gtk.gdk.Event
import org.gtk.gdk.KeyMatch
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 25 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ShortcutTrigger.html">
 *     GtkShortcutTrigger</a>
 */
open class ShortcutTrigger(
	val triggerPointer: GtkShortcutTrigger_autoptr,
) : KGObject(triggerPointer.reinterpret()), Comparable<ShortcutTrigger> {

	constructor(obj: KGObject) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_SHORTCUT_TRIGGER))

	override fun compareTo(other: ShortcutTrigger): Int =
		gtk_shortcut_trigger_compare(triggerPointer, other.triggerPointer)

	fun equal(other: ShortcutTrigger): Boolean =
		gtk_shortcut_trigger_equal(triggerPointer, other.triggerPointer).bool

	val hash: UInt
		get() = gtk_shortcut_trigger_hash(triggerPointer)

	// ignore gtk_shortcut_trigger_print as its for debug
	// ignore gtk_shortcut_trigger_print_label as it seems to be for debug

	fun toLabel(display: Display): String =
		gtk_shortcut_trigger_to_label(
			triggerPointer,
			display.displayPointer
		)!!.toKString()

	override fun toString(): String =
		gtk_shortcut_trigger_to_string(triggerPointer)!!.toKString()

	fun trigger(event: Event, enableMnemonics: Boolean) =
		KeyMatch.valueOf(
			gtk_shortcut_trigger_trigger(
				triggerPointer,
				event.eventPointer,
				enableMnemonics.gtk
			)
		)

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other == null) return false
		if (this::class != other::class) return false

		other as ShortcutTrigger

		return equal(other)
	}

	override fun hashCode(): Int =
		hash.toInt()

	companion object {
		inline fun GtkShortcutTrigger_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkShortcutTrigger_autoptr.wrap() =
			ShortcutTrigger(this)

		fun parseString(string: String): ShortcutTrigger? =
			gtk_shortcut_trigger_parse_string(string).wrap()
	}
}