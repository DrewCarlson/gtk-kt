package org.gtk.gtk.widgets.box

import glib.gint
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.windows.dialog.MessageDialog

/**
 * kotlinx-gtk
 * 27 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.InfoBar.html">
 *     GtkInfoBar</a>
 */
class InfoBar(
	val infoBarPointer: GtkInfoBar_autoptr,
) : Widget(infoBarPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_INFO_BAR))

	constructor() : this(gtk_info_bar_new()!!.reinterpret())

	// Ignored gtk_info_bar_new_with_buttons due to vararg

	fun addActionWidget(widget: Widget, responseID: Int) {
		gtk_info_bar_add_action_widget(
			infoBarPointer,
			widget.widgetPointer,
			responseID
		)
	}

	fun addButton(buttonText: String, responseID: Int) {
		gtk_info_bar_add_button(infoBarPointer, buttonText, responseID)
	}

	// Ignored gtk_info_bar_add_buttons due to vararg

	fun addChild(widget: Widget) {
		gtk_info_bar_add_child(infoBarPointer, widget.widgetPointer)
	}

	var messageType: MessageDialog.MessageType
		get() = MessageDialog.MessageType.valueOf(
			gtk_info_bar_get_message_type(
				infoBarPointer
			)
		)
		set(value) = gtk_info_bar_set_message_type(infoBarPointer, value.gtk)

	var revealed: Boolean
		get() = gtk_info_bar_get_revealed(infoBarPointer).bool
		set(value) = gtk_info_bar_set_revealed(
			infoBarPointer,
			value.gtk
		)

	var showCloseButton: Boolean
		get() = gtk_info_bar_get_show_close_button(infoBarPointer).bool
		set(value) = gtk_info_bar_set_show_close_button(
			infoBarPointer,
			value.gtk
		)

	fun removeActionWidget(widget: Widget) {
		gtk_info_bar_remove_action_widget(infoBarPointer, widget.widgetPointer)
	}

	fun removeChild(widget: Widget) {
		gtk_info_bar_remove_child(infoBarPointer, widget.widgetPointer)
	}

	fun response(responseID: Int) {
		gtk_info_bar_response(infoBarPointer, responseID)
	}

	fun setDefaultResponse(responseID: Int) {
		gtk_info_bar_set_default_response(infoBarPointer, responseID)
	}

	fun setResponseSensitive(responseID: Int, setting: Boolean) {
		gtk_info_bar_set_response_sensitive(
			infoBarPointer,
			responseID,
			setting.gtk
		)
	}

	fun addOnCloseCallback(action: TypedNoArgFunc<InfoBar>) =
		addSignalCallback(Signals.CLOSE, action, staticNoArgGCallback)

	fun addOnResponseCallback(action: InfoBar.(Int) -> Unit) =
		addSignalCallback(Signals.RESPONSE, action, staticResponseCallback)

	companion object {

		inline fun GtkInfoBar_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkInfoBar_autoptr.wrap() =
			InfoBar(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction {
					self: GtkInfoBar_autoptr,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<InfoBar>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()


		val staticResponseCallback: GCallback =
			staticCFunction {
					self: GtkInfoBar_autoptr,
					response_id: gint,
					data: gpointer,
				->
				data.asStableRef<InfoBar.(Int) -> Unit>()
					.get()
					.invoke(self.wrap(), response_id)
			}.reinterpret()
	}
}