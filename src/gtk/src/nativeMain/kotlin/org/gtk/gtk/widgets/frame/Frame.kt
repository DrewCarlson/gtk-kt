package org.gtk.gtk.widgets.frame

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 13 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Frame.html">GtkFrame</a>
 */
open class Frame(
	val framePointer: CPointer<GtkFrame>
) : Widget(framePointer.reinterpret()) {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_FRAME))

	constructor(
		label: String? = null
	) : this(gtk_frame_new(label)!!.reinterpret())

	var child: Widget?
		get() = gtk_frame_get_child(framePointer)?.wrap()
		set(value) = gtk_frame_set_child(framePointer, value?.widgetPointer)

	var label: String?
		get() = gtk_frame_get_label(framePointer)?.toKString()
		set(value) {
			gtk_frame_set_label(framePointer, value)
		}

	var labelAlign: Float
		get() = gtk_frame_get_label_align(framePointer)
		set(value) = gtk_frame_set_label_align(framePointer, value)

	var labelWidget: Widget?
		get() = gtk_frame_get_label_widget(framePointer)?.let { Widget(it) }
		set(value) = gtk_frame_set_label_widget(
			framePointer,
			value?.widgetPointer
		)
}