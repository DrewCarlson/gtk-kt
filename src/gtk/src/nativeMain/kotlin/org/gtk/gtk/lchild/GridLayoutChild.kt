package org.gtk.gtk.lchild

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 02 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GridLayoutChild.html">
 *     GtkGridLayoutChild</a>
 */
class GridLayoutChild(
	val gridPointer: GtkGridLayoutChild_autoptr,
) : LayoutChild(gridPointer.reinterpret()) {

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_GRID_LAYOUT_CHILD
	))

	var column: Int
		get() = gtk_grid_layout_child_get_column(gridPointer)
		set(value) = gtk_grid_layout_child_set_column(gridPointer, value)

	var columnSpan: Int
		get() = gtk_grid_layout_child_get_column_span(gridPointer)
		set(value) = gtk_grid_layout_child_set_column_span(gridPointer, value)

	var row: Int
		get() = gtk_grid_layout_child_get_row(gridPointer)
		set(value) = gtk_grid_layout_child_set_row(gridPointer, value)

	var rowSpan: Int
		get() = gtk_grid_layout_child_get_row_span(gridPointer)
		set(value) = gtk_grid_layout_child_set_row_span(gridPointer, value)
}