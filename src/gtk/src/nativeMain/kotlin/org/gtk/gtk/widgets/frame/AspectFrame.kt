package org.gtk.gtk.widgets.frame

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 20 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.AspectFrame.html">GtkAspectFrame</a>
 */
class AspectFrame(
	val aspectFramePointer: CPointer<GtkAspectFrame>
) : Widget(aspectFramePointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_ASPECT_FRAME))

	constructor(xAlign: Float, yAlign: Float, ratio: Float, obeyChild: Boolean) :
			this(gtk_aspect_frame_new(xAlign, yAlign, ratio, obeyChild.gtk)!!.reinterpret())

	var child: Widget?
		get() = gtk_aspect_frame_get_child(aspectFramePointer).wrap()
		set(value) = gtk_aspect_frame_set_child(aspectFramePointer, value?.widgetPointer)

	var obeyChild: Boolean
		get() = gtk_aspect_frame_get_obey_child(aspectFramePointer).bool
		set(value) = gtk_aspect_frame_set_obey_child(aspectFramePointer, value.gtk)

	var ratio: Float
		get() = gtk_aspect_frame_get_ratio(aspectFramePointer)
		set(value) = gtk_aspect_frame_set_ratio(aspectFramePointer, value)

	var xAlign: Float
		get() = gtk_aspect_frame_get_xalign(aspectFramePointer)
		set(value) = gtk_aspect_frame_set_xalign(aspectFramePointer, value)

	var yAlign: Float
		get() = gtk_aspect_frame_get_yalign(aspectFramePointer)
		set(value) = gtk_aspect_frame_set_yalign(aspectFramePointer, value)
}