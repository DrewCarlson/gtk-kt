package org.gtk.gtk.cellrenderer

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.Event
import org.gtk.gdk.Rectangle
import org.gtk.gdk.Rectangle.Companion.wrap
import org.gtk.glib.CStringPointer
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gobject.addSignalCallback
import org.gtk.gtk.CellEditable
import org.gtk.gtk.CellEditable.Companion.wrap
import org.gtk.gtk.Snapshot
import org.gtk.gtk.common.data.*
import org.gtk.gtk.common.data.Requisition.Companion.wrap
import org.gtk.gtk.common.enums.SizeRequestMode
import org.gtk.gtk.widgets.Widget

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.CellRenderer.html">
 *     GtkCellRenderer</a>
 */
open class CellRenderer(
	val cellRendererPointer: GtkCellRenderer_autoptr,
) : KGObject(cellRendererPointer.reinterpret()) {

	fun activate(
		event: Event,
		widget: Widget,
		path: String,
		backgroundArea: Rectangle,
		cellArea: Rectangle,
		flags: GtkCellRendererState,
	): Boolean =
		gtk_cell_renderer_activate(
			cellRendererPointer,
			event.eventPointer,
			widget.widgetPointer,
			path,
			backgroundArea.rectanglePointer,
			cellArea.rectanglePointer,
			flags
		).bool

	fun getAlignedArea(
		widget: Widget,
		flags: GtkCellRendererState,
		cellArea: Rectangle,
	): Rectangle = memScoped {
		val alignedArea = alloc<GdkRectangle>()

		gtk_cell_renderer_get_aligned_area(
			cellRendererPointer,
			widget.widgetPointer,
			flags,
			cellArea.rectanglePointer,
			alignedArea.ptr
		)

		alignedArea.ptr.wrap()
	}

	data class Alignment(
		val xAlign: Float,
		val yAlign: Float,
	)

	fun getAlignment(): Alignment = memScoped {
		val x = alloc<FloatVar>()
		val y = alloc<FloatVar>()

		gtk_cell_renderer_get_alignment(
			cellRendererPointer,
			x.ptr,
			y.ptr
		)

		Alignment(
			x.value,
			y.value
		)
	}

	var fixedSize: Dimensions
		get() = memScoped {
			val x = alloc<IntVar>()
			val y = alloc<IntVar>()

			gtk_cell_renderer_get_fixed_size(
				cellRendererPointer,
				x.ptr,
				y.ptr
			)

			Dimensions(
				x.value,
				y.value
			)
		}
		set(value) = gtk_cell_renderer_set_fixed_size(
			cellRendererPointer,
			value.width,
			value.height
		)

	var isExpanded: Boolean
		get() = gtk_cell_renderer_get_is_expanded(cellRendererPointer).bool
		set(value) = gtk_cell_renderer_set_is_expanded(
			cellRendererPointer,
			value.gtk
		)

	var isExpander: Boolean
		get() = gtk_cell_renderer_get_is_expander(cellRendererPointer).bool
		set(value) = gtk_cell_renderer_set_is_expander(
			cellRendererPointer,
			value.gtk
		)

	var padding: Coordinates
		get() = memScoped {
			val x = alloc<IntVar>()
			val y = alloc<IntVar>()

			gtk_cell_renderer_get_padding(
				cellRendererPointer,
				x.ptr,
				y.ptr
			)

			Coordinates(
				x.value,
				y.value
			)
		}
		set(value) = gtk_cell_renderer_set_padding(
			cellRendererPointer,
			value.x,
			value.y
		)

	fun getPreferredHeight(widget: Widget): PreferredHeight = memScoped {
		val minimumHeight = cValue<IntVar>()
		val naturalHeight = cValue<IntVar>()

		gtk_cell_renderer_get_preferred_height(
			cellRendererPointer,
			widget.widgetPointer,
			minimumHeight,
			naturalHeight
		)

		PreferredHeight(
			minimumHeight.ptr.pointed.value,
			naturalHeight.ptr.pointed.value
		)
	}

	fun getPreferredHeightForWidth(
		widget: Widget,
		width: Int,
	): PreferredHeight = memScoped {
		val minimumHeight = cValue<IntVar>()
		val naturalHeight = cValue<IntVar>()

		gtk_cell_renderer_get_preferred_height_for_width(
			cellRendererPointer,
			widget.widgetPointer,
			width,
			minimumHeight,
			naturalHeight
		)

		PreferredHeight(
			minimumHeight.ptr.pointed.value,
			naturalHeight.ptr.pointed.value
		)
	}

	fun getPreferredSize(widget: Widget): PreferredSize = memScoped {
		val min = cValue<GtkRequisition>()
		val nat = cValue<GtkRequisition>()

		gtk_cell_renderer_get_preferred_size(
			cellRendererPointer,
			widget.widgetPointer,
			min,
			nat
		)

		PreferredSize(
			min.ptr.wrap(),
			nat.ptr.wrap()
		)
	}

	fun getPreferredWidth(
		widget: Widget,
	): PreferredWidth =
		memScoped {
			val minimumWidth = cValue<IntVar>()
			val naturalWidth = cValue<IntVar>()

			gtk_cell_renderer_get_preferred_width(
				cellRendererPointer,
				widget.widgetPointer,
				minimumWidth,
				naturalWidth
			)

			PreferredWidth(
				minimumWidth.ptr.pointed.value,
				naturalWidth.ptr.pointed.value
			)
		}

	fun getPreferredWidthForHeight(
		widget: Widget,
		height: Int,
	): PreferredWidth =
		memScoped {
			val minimumWidth = cValue<IntVar>()
			val naturalWidth = cValue<IntVar>()

			gtk_cell_renderer_get_preferred_width_for_height(
				cellRendererPointer,
				widget.widgetPointer,
				height,
				minimumWidth,
				naturalWidth,
			)

			PreferredWidth(
				minimumWidth.ptr.pointed.value,
				naturalWidth.ptr.pointed.value
			)
		}

	val requestMode: SizeRequestMode
		get() = SizeRequestMode.valueOf(
			gtk_cell_renderer_get_request_mode(cellRendererPointer)
		)

	var sensitive: Boolean
		get() = gtk_cell_renderer_get_sensitive(cellRendererPointer).bool
		set(value) = gtk_cell_renderer_set_sensitive(
			cellRendererPointer,
			value.gtk
		)

	fun getState(
		widget: Widget,
		cellState: GtkCellRendererState,
	): GtkStateFlags =
		gtk_cell_renderer_get_state(
			cellRendererPointer,
			widget.widgetPointer,
			cellState
		)

	var visible: Boolean
		get() = gtk_cell_renderer_get_visible(cellRendererPointer).bool
		set(value) = gtk_cell_renderer_set_visible(
			cellRendererPointer,
			value.gtk
		)

	val isActivatable: Boolean
		get() = gtk_cell_renderer_is_activatable(cellRendererPointer).bool

	fun snapshot(
		snapshot: Snapshot,
		widget: Widget,
		backgroundArea: Rectangle,
		cellArea: Rectangle,
		flags: GtkCellRendererState,
	) {
		gtk_cell_renderer_snapshot(
			cellRendererPointer,
			snapshot.snapshotPointer,
			widget.widgetPointer,
			backgroundArea.rectanglePointer,
			cellArea.rectanglePointer,
			flags
		)
	}

	fun startEditing(
		event: Event?,
		widget: Widget,
		path: String,
		backgroundArea: Rectangle,
		cellArea: Rectangle,
		flags: GtkCellRendererState,
	): CellEditable? =
		gtk_cell_renderer_start_editing(
			cellRendererPointer,
			event?.eventPointer,
			widget.widgetPointer,
			path,
			backgroundArea.rectanglePointer,
			cellArea.rectanglePointer,
			flags
		).wrap()

	fun stopEditing(canceled: Boolean) {
		gtk_cell_renderer_stop_editing(cellRendererPointer, canceled.gtk)
	}

	fun addOnEditingCanceledCallback(action: TypedNoArgFunc<CellRenderer>) =
		addSignalCallback(
			Signals.EDITING_CANCELED,
			action,
			staticNoArgGCallback
		)

	fun addOnEditingStartedCallback(action: CellRendererEditingStartedFunc)=
		addSignalCallback(
			Signals.EDITING_STARTED,
			action,
			staticStartedCallback
		)

	companion object {
		inline fun GtkCellRenderer_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkCellRenderer_autoptr.wrap() =
			CellRenderer(this)


		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkCellRenderer_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<CellRenderer>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticStartedCallback: GCallback =
			staticCFunction {
					self: GtkCellRenderer_autoptr,
					editable: GtkCellEditable_autoptr,
					path: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<CellRendererEditingStartedFunc>()
					.get()
					.invoke(
						self.wrap(),
						editable.wrap(),
						path.toKString()
					)
			}.reinterpret()
	}
}

typealias CellRendererEditingStartedFunc =
		CellRenderer.(
			editable: CellEditable,
			path: String,
		) -> Unit