package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.memScoped
import org.gtk.glib.*
import org.gtk.glib.Variant.Companion.wrap
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.PaperSize.Companion.wrap

/**
 * kotlinx-gtk
 * 09 / 06 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.PageSetup.html">
 *     GtkPageSetup</a>
 */
class PageSetup(
	val pageSetupPointer: CPointer<GtkPageSetup>,
) {

	constructor() :
			this(gtk_page_setup_new()!!)

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_PAGE_SETUP))

	@Throws(KGError::class)
	constructor(fileName: String) :
			this(
				memScoped {
					val error = allocateGErrorPtr()

					val ptr = gtk_page_setup_new_from_file(fileName, error)

					error.unwrap()

					ptr!!
				}
			)

	constructor(variant: Variant) :
			this(gtk_page_setup_new_from_gvariant(
				variant.variantPointer
			)!!)

	constructor(keyFile: KeyFile, groupName: String) :
			this(
				memScoped {
					val error = allocateGErrorPtr()

					val ptr = gtk_page_setup_new_from_key_file(
						keyFile.structPointer,
						groupName,
						error
					)

					error.unwrap()

					ptr!!
				}
			)

	fun copy(): PageSetup =
		gtk_page_setup_copy(pageSetupPointer)!!.wrap()

	fun getBottomMargin(unit: MeasurementUnit): Double =
		gtk_page_setup_get_bottom_margin(pageSetupPointer, unit.gtk)

	fun getLeftMargin(unit: MeasurementUnit): Double =
		gtk_page_setup_get_left_margin(pageSetupPointer, unit.gtk)

	var orientation: PageOrientation
		get() = PageOrientation.valueOf(
			gtk_page_setup_get_orientation(pageSetupPointer)
		)
		set(value) = gtk_page_setup_set_orientation(pageSetupPointer, value.gtk)

	fun getPageHeight(unit: MeasurementUnit): Double =
		gtk_page_setup_get_page_height(pageSetupPointer, unit.gtk)

	fun getPageWidth(unit: MeasurementUnit): Double =
		gtk_page_setup_get_page_width(pageSetupPointer, unit.gtk)

	fun getPaperHeight(unit: MeasurementUnit): Double =
		gtk_page_setup_get_paper_height(pageSetupPointer, unit.gtk)

	var paperSize: PaperSize
		get() = gtk_page_setup_get_paper_size(pageSetupPointer)!!.wrap()
		set(value) = gtk_page_setup_set_paper_size(
			pageSetupPointer,
			value.sizePointer
		)

	fun getPaperWidth(unit: MeasurementUnit): Double =
		gtk_page_setup_get_paper_width(pageSetupPointer, unit.gtk)

	fun getRightMargin(unit: MeasurementUnit): Double =
		gtk_page_setup_get_right_margin(pageSetupPointer, unit.gtk)

	fun getTopMargin(unit: MeasurementUnit): Double =
		gtk_page_setup_get_top_margin(pageSetupPointer, unit.gtk)

	fun loadFile(fileName: String): Boolean =
		memScoped {
			val error = allocateGErrorPtr()

			val result = gtk_page_setup_load_file(
				pageSetupPointer,
				fileName,
				error
			)

			error.unwrap()

			result.bool
		}

	fun loadKeyFile(keyFile: KeyFile, groupName: String): Boolean =
		memScoped {
			val error = allocateGErrorPtr()

			val result = gtk_page_setup_load_key_file(
				pageSetupPointer,
				keyFile.structPointer,
				groupName,
				error
			)

			error.unwrap()

			result.bool
		}

	fun setBottomMargin(unit: MeasurementUnit, margin: Double) {
		gtk_page_setup_set_bottom_margin(pageSetupPointer, margin, unit.gtk)
	}

	fun setLeftMargin(unit: MeasurementUnit, margin: Double) {
		gtk_page_setup_set_left_margin(pageSetupPointer, margin, unit.gtk)
	}

	fun setPaperSizeAndDefaultMargins(size: PaperSize) {
		gtk_page_setup_set_paper_size_and_default_margins(
			pageSetupPointer,
			size.sizePointer
		)
	}

	fun setRightMargin(unit: MeasurementUnit, margin: Double) {
		gtk_page_setup_set_right_margin(pageSetupPointer, margin, unit.gtk)
	}

	fun setTopMargin(unit: MeasurementUnit, margin: Double) {
		gtk_page_setup_set_top_margin(pageSetupPointer, margin, unit.gtk)
	}

	fun toFile(fileName: String): Boolean =
		memScoped {
			val error = allocateGErrorPtr()

			val result = gtk_page_setup_to_file(
				pageSetupPointer,
				fileName,
				error
			)

			error.unwrap()

			result.bool
		}

	fun toVariant(): Variant =
		gtk_page_setup_to_gvariant(pageSetupPointer)!!.wrap()

	fun toKeyFile(keyFile: KeyFile, groupName: String) {
		gtk_page_setup_to_key_file(
			pageSetupPointer,
			keyFile.structPointer,
			groupName
		)
	}

	companion object {
		inline fun CPointer<GtkPageSetup>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkPageSetup>.wrap() =
			PageSetup(this)
	}

}