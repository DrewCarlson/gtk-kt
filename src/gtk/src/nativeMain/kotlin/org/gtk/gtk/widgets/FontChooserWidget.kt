package org.gtk.gtk.widgets

import gtk.GtkFontChooser
import gtk.GtkFontChooserWidget_autoptr
import gtk.gtk_font_chooser_widget_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gtk.FontChooser

/**
 * 24 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.FontChooserWidget.html">
 *     GtkFontChooserWidget</a>
 */
class FontChooserWidget(
	val fontChooserWidgetPointer: GtkFontChooserWidget_autoptr,
) : Widget(fontChooserWidgetPointer.reinterpret()), FontChooser {
	override val fontChooserPointer: CPointer<GtkFontChooser> by lazy {
		fontChooserWidgetPointer.reinterpret()
	}

	constructor() : this(gtk_font_chooser_widget_new()!!.reinterpret())

}