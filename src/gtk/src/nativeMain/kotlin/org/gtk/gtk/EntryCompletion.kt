package org.gtk.gtk

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import org.gtk.glib.CStringPointer
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.*
import org.gtk.gtk.TreeIter.Companion.wrap
import org.gtk.gtk.TreeModel.Companion.wrap
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.Widget.Companion.wrap

/**
 * kotlinx-gtk
 *
 * 03 / 07 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.EntryCompletion.html">
 *     GtkEntryCompletion</a>
 */
class EntryCompletion(
	val completionPointer: GtkEntryCompletion_autoptr,
) : KGObject(completionPointer.reinterpret()), Buildable, CellLayout {

	constructor() : this(gtk_entry_completion_new()!!)

	constructor(area: CellArea) : this(gtk_entry_completion_new_with_area(
		area.cellAreaPointer
	)!!)

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_ENTRY_COMPLETION
	))

	fun complete() {
		gtk_entry_completion_complete(completionPointer)
	}

	fun computePrefix(key: String): String? =
		gtk_entry_completion_compute_prefix(
			completionPointer,
			key
		)?.toKString()

	val completionPrefix: String?
		get() = gtk_entry_completion_get_completion_prefix(
			completionPointer
		)?.toKString()

	val entry: Widget
		get() = gtk_entry_completion_get_entry(completionPointer)!!.wrap()

	var inlineCompletion: Boolean
		get() = gtk_entry_completion_get_inline_completion(
			completionPointer
		).bool
		set(value) = gtk_entry_completion_set_inline_completion(
			completionPointer,
			value.gtk
		)

	var inlineSelection: Boolean
		get() = gtk_entry_completion_get_inline_selection(
			completionPointer
		).bool
		set(value) = gtk_entry_completion_set_inline_selection(
			completionPointer,
			value.gtk
		)

	var minimumKeyLength: Int
		get() = gtk_entry_completion_get_minimum_key_length(
			completionPointer
		)
		set(value) = gtk_entry_completion_set_minimum_key_length(
			completionPointer,
			value
		)

	var model: TreeModel?
		get() = gtk_entry_completion_get_model(
			completionPointer
		).wrap()
		set(value) = gtk_entry_completion_set_model(
			completionPointer,
			value?.treeModelPointer
		)

	var popupCompletion: Boolean
		get() = gtk_entry_completion_get_popup_completion(
			completionPointer
		).bool
		set(value) = gtk_entry_completion_set_popup_completion(
			completionPointer,
			value.gtk
		)

	var popupSetWidth: Boolean
		get() = gtk_entry_completion_get_popup_set_width(
			completionPointer
		).bool
		set(value) = gtk_entry_completion_set_popup_set_width(
			completionPointer,
			value.gtk
		)

	var popupSingleMatch: Boolean
		get() = gtk_entry_completion_get_popup_single_match(
			completionPointer
		).bool
		set(value) = gtk_entry_completion_set_popup_single_match(
			completionPointer,
			value.gtk
		)

	var textColumn: Int
		get() = gtk_entry_completion_get_text_column(
			completionPointer
		)
		set(value) = gtk_entry_completion_set_text_column(
			completionPointer,
			value
		)

	fun addOnCursorOnMatchCallback(action: EntryCompletionMatchFunc) =
		addSignalCallback(Signals.CURSOR_ON_MATCH, action, staticMatchFunc)

	fun addOnInsertPrefixCallback(action: EntryCompletionInsertPrefixFunc) =
		addSignalCallback(Signals.INSERT_PREFIX, action, staticInsertFunc)

	fun addOnMatchSelectedCallback(action: EntryCompletionMatchFunc)=
		addSignalCallback(Signals.MATCH_SELECTED, action, staticMatchFunc)

	fun addOnNoMatchesCallback(action: TypedNoArgFunc<EntryCompletion>)=
		addSignalCallback(Signals.NO_MATCHES, action, staticNoArgFunc)


	companion object {
		inline fun GtkEntryCompletion_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkEntryCompletion_autoptr.wrap() =
			EntryCompletion(this)

		private val staticNoArgFunc: GCallback =
			staticCFunction {
					self: GtkEntryCompletion_autoptr,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<EntryCompletion>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticInsertFunc: GCallback =
			staticCFunction {
					self: GtkEntryCompletion_autoptr,
					prefix: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<EntryCompletionInsertPrefixFunc>()
					.get()
					.invoke(self.wrap(), prefix.toKString())
					.gtk
			}.reinterpret()

		private val staticMatchFunc: GCallback =
			staticCFunction {
					self: GtkEntryCompletion_autoptr,
					model: GtkTreeModel_autoptr,
					iter: GtkTreeIter_autoptr,
					data: gpointer,
				->
				data.asStableRef<EntryCompletionMatchFunc>()
					.get()
					.invoke(self.wrap(), model.wrap(), iter.wrap())
					.gtk
			}.reinterpret()
	}

	override val buildablePointer: GtkBuildable_autoptr by lazy {
		completionPointer.reinterpret()
	}
	override val cellLayoutHolder: GtkCellLayout_autoptr by lazy {
		completionPointer.reinterpret()
	}
}

typealias EntryCompletionMatchFunc =
		EntryCompletion.(
			model: TreeModel,
			iter: TreeIter,
		) -> Boolean

typealias EntryCompletionInsertPrefixFunc =
		EntryCompletion.(
			prefix: String,
		) -> Boolean


