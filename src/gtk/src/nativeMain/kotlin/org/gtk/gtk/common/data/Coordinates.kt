package org.gtk.gtk.common.data

/**
 * 15 / 12 / 2021
 *
 * Represents any combination of X,Y
 */
data class Coordinates(
	val x: Int,
	val y: Int,
)

/**
 * 21 / 12 / 2021
 *
 * Represents any combination of X,Y
 *
 * Is more precise via double
 */
data class PreciseCoordinates(
	val x: Double,
	val y: Double,
)
