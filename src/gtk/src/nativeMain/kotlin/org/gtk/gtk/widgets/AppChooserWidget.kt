package org.gtk.gtk.widgets

import gio.GAppInfo_autoptr
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import org.gtk.gio.AppInfo
import org.gtk.gio.AppInfo.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals.APPLICATION_ACTIVATED
import org.gtk.gobject.Signals.APPLICATION_SELECTED
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.AppChooser

/**
 * 19 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.AppChooserWidget.html">
 *     GtkAppChooserWidget</a>
 */
class AppChooserWidget(
	val appWidgetPointer: GtkAppChooserWidget_autoptr,
) : Widget(appWidgetPointer.reinterpret()), AppChooser {
	override val appChooserPointer: GtkAppChooser_autoptr by lazy {
		appWidgetPointer.reinterpret()
	}

	constructor(contentType: String) :
			this(gtk_app_chooser_widget_new(contentType)!!.reinterpret())

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_APP_CHOOSER_WIDGET))

	var defaultText: String?
		get() = gtk_app_chooser_widget_get_default_text(
			appWidgetPointer
		)?.toKString()
		set(value) = gtk_app_chooser_widget_set_default_text(
			appWidgetPointer,
			value
		)

	var showAll: Boolean
		get() = gtk_app_chooser_widget_get_show_all(appWidgetPointer).bool
		set(value) = gtk_app_chooser_widget_set_show_all(
			appWidgetPointer,
			value.gtk
		)

	var showDefault: Boolean
		get() = gtk_app_chooser_widget_get_show_default(appWidgetPointer).bool
		set(value) = gtk_app_chooser_widget_set_show_default(
			appWidgetPointer,
			value.gtk
		)

	var showFallback: Boolean
		get() = gtk_app_chooser_widget_get_show_fallback(appWidgetPointer).bool
		set(value) = gtk_app_chooser_widget_set_show_fallback(
			appWidgetPointer,
			value.gtk
		)

	var showOther: Boolean
		get() = gtk_app_chooser_widget_get_show_other(appWidgetPointer).bool
		set(value) = gtk_app_chooser_widget_set_show_other(
			appWidgetPointer,
			value.gtk
		)

	var showRecommended: Boolean
		get() = gtk_app_chooser_widget_get_show_recommended(
			appWidgetPointer
		).bool
		set(value) = gtk_app_chooser_widget_set_show_recommended(
			appWidgetPointer,
			value.gtk
		)

	fun addOnApplicationActivatedCallback(
		action: AppChooserWidgetApplicationFunc,
	) = addSignalCallback(
		APPLICATION_ACTIVATED,
		action,
		staticApplicationFunction
	)

	fun addOnApplicationSelectedCallback(
		action: AppChooserWidgetApplicationFunc,
	) = addSignalCallback(
		APPLICATION_SELECTED,
		action,
		staticApplicationFunction
	)

	companion object {
		private val staticApplicationFunction: GCallback =
			staticCFunction {
					self: GtkAppChooserWidget_autoptr,
					application: GAppInfo_autoptr,
					data: gpointer,
				->
				data.asStableRef<AppChooserWidgetApplicationFunc>()
					.get()
					.invoke(self.wrap(), application.wrap())
			}.reinterpret();

		inline fun GtkAppChooserWidget_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkAppChooserWidget_autoptr.wrap() =
			AppChooserWidget(this)
	}

}

typealias AppChooserWidgetApplicationFunc =
		AppChooserWidget.(
			application: AppInfo,
		) -> Unit