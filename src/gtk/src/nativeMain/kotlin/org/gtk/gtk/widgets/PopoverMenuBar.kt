package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gio.MenuModel
import org.gtk.gio.MenuModel.Companion.wrap
import org.gtk.glib.bool

/**
 * 24 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.PopoverMenuBar.html">
 *     GtkPopoverMenuBar</a>
 */
class PopoverMenuBar(
	val popoverMenuBarPointer: CPointer<GtkPopoverMenuBar>,
) : Widget(popoverMenuBarPointer.reinterpret()) {

	constructor(model: MenuModel?) :
			this(
				gtk_popover_menu_bar_new_from_model(
					model?.menuModelPointer
				)!!.reinterpret()
			)

	fun addChild(child: Widget, id: String) =
		gtk_popover_menu_bar_add_child(
			popoverMenuBarPointer,
			child.widgetPointer,
			id
		).bool

	var menuModel: MenuModel
		get() = gtk_popover_menu_bar_get_menu_model(
			popoverMenuBarPointer
		)!!.wrap()
		set(value) = gtk_popover_menu_bar_set_menu_model(
			popoverMenuBarPointer,
			value.menuModelPointer
		)

	fun removeChild(child: Widget): Boolean =
		gtk_popover_menu_bar_remove_child(
			popoverMenuBarPointer,
			child.widgetPointer
		).bool
}