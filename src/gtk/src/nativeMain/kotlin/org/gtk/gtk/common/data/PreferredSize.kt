package org.gtk.gtk.common.data

data class PreferredSize(
	val minimmum: Requisition,
	val maximum: Requisition,
)