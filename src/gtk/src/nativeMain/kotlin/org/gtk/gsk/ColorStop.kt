package org.gtk.gsk

import gtk.GskColorStop
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.ptr
import org.gtk.gdk.RGBA
import org.gtk.gdk.RGBA.Companion.wrap

/**
 * kotlinx-gtk
 *
 * 13 / 10 / 2021
 *
 * @see <a href="https://docs.gtk.org/gsk4/struct.ColorStop.html">GskColorStop</a>
 */
class ColorStop(val colorStopPointer: CPointer<GskColorStop>) {
	var offset: Float
		get() = colorStopPointer.pointed.offset
		set(value) {
			colorStopPointer.pointed.offset = value
		}

	var color: RGBA
		get() = colorStopPointer.pointed.color.ptr.wrap()
		set(value) {
			colorStopPointer.pointed.color.apply {
				red = value.red
				green = value.green
				blue = value.blue
				alpha = value.alpha
			}
		}
}