plugins {
	kotlin("multiplatform")
	`maven-publish`
	signing
	`dokka-script`
}

version = "1.0.0-alpha1"
description = "Kotlin bindings for GTK4, targets 4.4.1"

kotlin {
	native {
		val main by compilations.getting
		val gtk by main.cinterops.creating
		//val gtkunixprint by main.cinterops.creating

		binaries {
			sharedLib()
		}
	}

	sourceSets {
		val nativeMain by getting {
			dependencies {
				implementation(project(":src:gio"))
				implementation(project(":src:cairo"))
				implementation(project(":src:pango"))
				implementation(project(":src:gobject"))
				implementation(project(":src:glib"))
				implementation(project(":src:gdk-pixbuf"))
			}
		}
	}
}

apply(plugin = "native-publish")