plugins {
	kotlin("multiplatform")
	`maven-publish`
	signing
	`dokka-script`
}

version = "2.68.0-alpha0"
description = "Kotlin bindings for Gio, targets 2.70.2"

kotlin {
    native {
        val main by compilations.getting
        val gio by main.cinterops.creating

        binaries {
            sharedLib()
        }
    }

    sourceSets {
        val nativeMain by getting {
            dependencies {
                implementation(project(":src:glib"))
                implementation(project(":src:gobject"))
            }
        }
    }
}

apply(plugin = "native-publish")
