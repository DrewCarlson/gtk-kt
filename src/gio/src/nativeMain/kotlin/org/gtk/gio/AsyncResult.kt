package org.gtk.gio

import gio.GAsyncReadyCallback
import gio.GAsyncResult_autoptr
import gio.g_async_result_get_source_object
import gio.g_async_result_get_user_data
import glib.gpointer
import gobject.GObject_autoptr
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.staticCFunction
import org.gtk.gio.AsyncResult.Companion.wrap
import org.gtk.gobject.KGObject.Companion.wrap

/**
 * kotlinx-gtk
 *
 * 05 / 07 / 2021
 *
 * @see <a href=""></a>
 */
class AsyncResult(val asyncResultPointer: GAsyncResult_autoptr) {
	val userData: gpointer?
		get() = g_async_result_get_user_data(asyncResultPointer)

	val sourceObject: GObject_autoptr?
		get() = g_async_result_get_source_object(asyncResultPointer)

	companion object {
		fun GAsyncResult_autoptr?.wrap() =
			this?.wrap()

		inline fun GAsyncResult_autoptr.wrap() =
			AsyncResult(this)
	}
}

internal val staticAsyncReadyCallback: GAsyncReadyCallback =
	staticCFunction { sourceObject: GObject_autoptr?, res: GAsyncResult_autoptr?, data: gpointer? ->
		data?.asStableRef<AsyncReadyCallback>()?.let {
			it.get().invoke(sourceObject.wrap(), res!!.wrap())
			it.dispose()
		}
		Unit
	}