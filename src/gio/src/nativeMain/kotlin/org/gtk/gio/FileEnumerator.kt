package org.gtk.gio

import gio.GFileEnumerator
import gio.GFileEnumerator_autoptr
import gio.g_file_enumerator_get_child
import gio.g_file_enumerator_next_file
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.memScoped
import org.gtk.gio.File.Companion.wrap
import org.gtk.gio.FileInfo.Companion.wrap
import org.gtk.glib.KGError
import org.gtk.glib.allocateGErrorPtr
import org.gtk.glib.unwrap

/**
 * @see <a href="https://docs.gtk.org/gio/class.FileEnumerator.html">
 *     GFileEnumerator</a>
 */
class FileEnumerator(val fileEnumeratorPointer: GFileEnumerator_autoptr) {

	@Throws(KGError::class)
	fun nextFile(
		cancellable: Cancellable? = null,
	): FileInfo? =
		memScoped {
			val err = allocateGErrorPtr()

			val r = g_file_enumerator_next_file(
				fileEnumeratorPointer,
				cancellable?.cancellablePointer,
				err
			)

			err.unwrap()

			r.wrap()
		}

	fun getChild(fileInfo: FileInfo): File =
		g_file_enumerator_get_child(
			fileEnumeratorPointer,
			fileInfo.fileInfoPointer
		)!!.wrap()

	companion object {
		inline fun CPointer<GFileEnumerator>?.wrap() =
			this?.wrap()

		inline fun CPointer<GFileEnumerator>.wrap() =
			FileEnumerator(this)
	}
}