package org.gtk.gio

import gio.*
import glib.GVariant
import glib.gcharVar
import kotlinx.cinterop.*
import org.gtk.glib.*
import org.gtk.glib.Variant.Companion.wrap
import org.gtk.glib.VariantType.Companion.wrap

/**
 * kotlinx-gtk
 *
 * 23 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/iface.Action.html">GAction</a>
 */
interface Action {
	val actionPointer: CPointer<GAction>

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Action.get_name.html">
	 *     g_action_get_name</a>
	 */
	val name: String
		get() = g_action_get_name(actionPointer)!!.toKString()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Action.get_parameter_type.html">
	 *     g_action_get_parameter_type</a>
	 */
	val parameterType: VariantType?
		get() = g_action_get_parameter_type(actionPointer).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Action.get_state_type.html">
	 *     g_action_get_state_type</a>
	 */
	val stateType: VariantType?
		get() = g_action_get_state_type(actionPointer).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Action.get_state_hint.html">
	 *     g_action_get_state_hint</a>
	 */
	val stateHint: Variant?
		get() = g_action_get_state_hint(actionPointer).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Action.get_enabled.html">
	 *     g_action_get_enabled</a>
	 */
	val enabled: Boolean
		get() = g_action_get_enabled(actionPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Action.get_state.html">
	 *     g_action_get_state</a>
	 */
	val state: Variant?
		get() = g_action_get_state(actionPointer).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Action.change_state.html">
	 *     g_action_change_state</a>
	 */
	fun changeState(value: Variant) {
		g_action_change_state(actionPointer, value.variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Action.activate.html">
	 *     g_action_activate</a>
	 */
	fun activate(parameter: Variant?) {
		g_action_activate(actionPointer, parameter?.variantPointer)
	}


	companion object {
		private class ImplAction(override val actionPointer: CPointer<GAction>) : Action

		inline fun CPointer<GAction>?.wrap() =
			this?.wrap()

		fun CPointer<GAction>.wrap(): Action =
			ImplAction(this)

		/**
		 * @see <a href="https://docs.gtk.org/gio/type_func.Action.name_is_valid.html">
		 *     g_action_name_is_valid</a>
		 */
		fun isNameValid(actionName: String): Boolean =
			g_action_name_is_valid(actionName).bool

		/**
		 * @see <a href="https://docs.gtk.org/gio/type_func.Action.parse_detailed_name.html">
		 *     g_action_print_detailed_name</a>
		 */
		fun printDetailedName(
			actionName: String,
			variant: Variant? = null
		): String =
			g_action_print_detailed_name(
				actionName,
				variant?.variantPointer
			)!!.toKString()

		/**
		 * @see <a href="https://docs.gtk.org/gio/type_func.Action.print_detailed_name.html">
		 *     g_action_parse_detailed_name</a>
		 */
		@Throws(KGError::class)
		fun parseDetailedName(
			detailedName: String,
			receiveParsed: (actionName: String, targetValue: Variant) -> Unit
		) {
			memScoped {
				val err = allocateGErrorPtr()

				val actionName = cValue<CPointerVar<gcharVar>>()
				val targetValue = cValue<CPointerVar<GVariant>>()

				g_action_parse_detailed_name(
					detailedName,
					actionName,
					targetValue,
					err
				)

				err.unwrap()

				receiveParsed(
					actionName.ptr.pointed.value!!.toKString(),
					targetValue.ptr.pointed.value!!.wrap()
				)
			}
		}
	}
}
