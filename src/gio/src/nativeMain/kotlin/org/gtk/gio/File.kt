package org.gtk.gio

import gio.*
import glib.GError
import glib.g_free
import kotlinx.cinterop.*
import org.gtk.gio.FileEnumerator.Companion.wrap
import org.gtk.gio.FileOutputStream.Companion.wrap
import org.gtk.glib.*
import org.gtk.gobject.KGObject

/**
 * 08 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/iface.File.html">GFile</a>
 */
class File(
	val filePointer: GFile_autoptr,
) : KGObject(filePointer.reinterpret()) {

	@Throws(KGError::class)
	fun appendTo(
		flags: GFileCreateFlags = G_FILE_CREATE_NONE,
		cancellable: KGCancellable? = null,
	): FileOutputStream =
		memScoped {
			val err = allocateGErrorPtr()

			val result = g_file_append_to(
				filePointer,
				flags,
				cancellable?.cancellablePointer,
				err
			)

			err.unwrap()

			result!!.wrap()
		}


	@Throws(KGError::class)
	fun enumerateChildren(
		attributes: String? = null,
		flags: GFileQueryInfoFlags = G_FILE_QUERY_INFO_NONE,
		cancellable: KGCancellable? = null,
	): FileEnumerator =
		memScoped {
			val err = allocateGErrorPtr()

			val r = g_file_enumerate_children(filePointer,
				attributes,
				flags,
				cancellable?.cancellablePointer,
				err
			)

			err.unwrap()

			r!!.wrap()
		}

	val baseName: String?
		get() = g_file_get_basename(filePointer)?.use { it.toKString() }

	fun peekPath(): String? =
		g_file_peek_path(filePointer)?.toKString()

	val parent: File?
		get() = g_file_get_parent(filePointer).wrap()

	val path: String?
		get() = g_file_get_path(filePointer)?.toKString()

	@Throws(KGError::class)
	fun loadContents(cancellable: KGCancellable? = null): String? {
		memScoped {
			val contents = alloc<CPointerVarOf<CPointer<ByteVar>>>()
			val err = allocPointerTo<GError>().ptr

			val r = g_file_load_contents(
				filePointer,
				cancellable?.cancellablePointer,
				contents.ptr,
				null,
				null,
				err
			).bool

			err.unwrap()

			return if (r) {
				val result = contents.ptr[0]?.toKString()?.removeSuffix("\n")
				result
			} else null
		}
	}

	companion object {
		inline fun GFile_autoptr?.wrap() =
			this?.wrap()

		inline fun GFile_autoptr.wrap() =
			File(this)

		fun Array<File>.toCArray(): CPointer<CPointerVar<GFile>> =
			memScoped {
				allocArrayOf(this@toCArray.map { it.filePointer })
			}

		fun newForPath(path: String): File =
			g_file_new_for_path(path)!!.wrap()
	}
}