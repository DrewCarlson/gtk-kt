import org.gtk.glib.KList
import org.gtk.gobject.KGObject

fun <T : KGObject> List<T>.toGList(): KList {
	val kList = KList()
	forEachIndexed { i, it ->
		if (i == 0)
			kList.insert(i, it.pointer)
		else
			kList.append(it.pointer)
	}
	return kList
}