plugins {
	kotlin("multiplatform")
	`maven-publish`
	signing
	`dokka-script`
}

version = "0.1.0-alpha1"
description = "Kotlin bindings for Cairo, targets 1.17.4"

kotlin {
	native {
		val main by compilations.getting
		val cairo by main.cinterops.creating

		binaries {
			sharedLib()
		}
	}

	sourceSets {
		val nativeMain by getting {
			dependencies {
				implementation(project(":src:glib"))
			}
		}
	}
}

apply(plugin = "native-publish")